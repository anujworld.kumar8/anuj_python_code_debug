def keep_last_occurrence(my_list):
    seen = set()
    result = []

    for item in reversed(my_list):
        if item not in seen:
            result.append(item)
            seen.add(item)

    return list(reversed(result))


my_list1 = [1, 2, 1, 4, 5, 6, 4, 5]
my_list2 = [1, 3, 4, 3, 2, 5, 2]

print(keep_last_occurrence(my_list1))
print(keep_last_occurrence(my_list2))

x=8
y=9

# swap Using 3rd variable
temp = x
x=y
y=temp
print("after swapping x= ", x)
print("after swapping y =", y)

# swap Without using third variable

a=7
b=3
a,b = b,a
print("after swapping a = ", a)
print("after swapping b =", b)

# swap With  mathematical operation (+,-)
m=5
n=6
m=m+n
n=m-n
m=m-n
print("after swapping m = ", m)
print("after swapping n =", n)

# swap With mathematical operation(*,/)
p = 3
q = 5
p = p*q
q=p//q
p=p//q
print("after swapping p= ", p)
print("after swapping q =", q)

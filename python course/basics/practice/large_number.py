# Given a list of non negative integer num, arrange them such that they form the largest number and return it.
# since the result may be large so return a string instead of integer.
'''
Exapmle:
input : num = [10,2]
output: "210"
''' 

my_lst = [9, 34, 30, 5, 85,89,12]
temp = 0
temp_lst = []
for i in my_lst:
    while i!=0:
        r = i % 10
        temp_lst.append(r)
        i = i//10
temp_lst.sort(reverse=True)
temp_str = ''
for i in temp_lst:
    temp_str +=str(i) 
print(temp_str)
    

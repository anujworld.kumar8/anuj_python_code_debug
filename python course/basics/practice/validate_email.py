# Enter your code here. Read input from STDIN. Print output to STDOUT
# def validate_email(email_str: str):
#     User_name = email_str.split("@")
    
    
         
         
    
    
    
    
    
# number_of_email = int(input())
# for i in range(number_of_email):
#     email_str = input()
# validate_email(email_str)

import re
import email.utils

# Define a regex for validating the email
def is_valid_email(email):
    # Validate based on given criteria for username, domain, and extension
    pattern = r'^[a-zA-Z][\w\.-]+@[a-zA-Z]+\.[a-zA-Z]{1,3}$'
    return re.match(pattern, email) is not None

# Read number of inputs
n = int(input())

# Process each line
for _ in range(n):
    input_line = input().strip()  # Read input and strip any extra whitespace
    name, email_address = email.utils.parseaddr(input_line)  # Parse name and email

    if is_valid_email(email_address):  # Check if the email is valid
        print(email.utils.formataddr((name, email_address)))  # Print in correct format
        print(email.utils.formataddr((name, email_address)))
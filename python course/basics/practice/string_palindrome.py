def string_palindrome(my_str):
    if my_str[::-1] == my_str[:]:
        print("Its a palindrome")
    else:
        print("String is not a palindrome")

my_str = "MADAM"
string_palindrome(my_str)

str2 = "this is string"
result = str2.reversed()
print(result)
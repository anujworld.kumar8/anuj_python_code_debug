# Sort a list without using sort function
def sort_list_without_method(my_list: list)-> list:
    # sorted_list = []
    for i in range(len(my_list)):
        for j in range(i+1, len(my_list)):
            if my_list[i] > my_list[j]:
                my_list[i], my_list[j] = my_list[j], my_list[i]
                
    return my_list       
            
my_list = [7,96,5,8,1,978]
print(sort_list_without_method(my_list))
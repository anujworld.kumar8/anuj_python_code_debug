'''Q13. Ask a string from user. Print the count of capital alphabets, small
alphabets, spaces, digits and then symbols (whatever are left, count them
in symbols).'''

def count_character(my_str: str):
    count_capital_alphabets=0
    count_small_alphabets=0
    count_space=0
    count_digits=0
    count_symbols=0
    for i in my_str:
        if ord(i) >=97 and ord(i) <=122:
            count_small_alphabets +=1
        elif ord(i) >=65 and ord(i) <=90:
            count_capital_alphabets+=1
        elif ord(i) >= 58 and ord(i)<=64:
            count_symbols+=1
        elif ord(i) >=33 and ord(i)<=47:
            count_symbols+=1
        elif ord(i) >=48 and ord(i)<=57:
            count_digits+=1
        elif ord(i)==32:
            count_space+=1
    print(f"Total capital Alphabets : {count_capital_alphabets}")    
    print(f"Total small Alphabets : {count_small_alphabets}")    
    print(f"Total spaces : {count_space}")    
    print(f"Total digits : {count_digits}")    
    print(f"Total symbols : {count_symbols}")   
       
my_str = "My name is Anuj Kumar, My gmail id is Anuj1024@gmail.com and the password is Aa@06120612"
count_character(my_str)    
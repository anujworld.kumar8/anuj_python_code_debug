student = {
    "name":"Aman",
    "age":28,
    "gender":"Male",
    "marks":[89,65,87,64,88],
    "location":"Patna",
    "Branch":"ME"
}
student["Branch"] = "CSE" 
# This line of code is updating the value associated with the key "Branch" in the `student`
# dictionary. It is changing the value from "ME" to "CSE". After this line is executed, the dictionary
# will now have the updated value for the key "Branch".
student["marks"] = [89,75,87,64,8]

student["id"] = 612 
# The line `student["id"] = 612` is adding a new key-value pair to the `student` dictionary. It is
# assigning the value `612` to the key `"id"`. After this line is executed, the `student` dictionary
# will have a new key `"id"` with the value `612`.
print(student)

del student["marks"]
# `del student["marks"]` is deleting the key-value pair associated with the key "marks" from the
# `student` dictionary. After this line is executed, the key "marks" and its corresponding value will
# be removed from the dictionary.
print(student)

student1 = {
    "name":"Aman",
    "age":28,
    "gender":"Male",
    "marks":[89,65,87,64,88],
    "location":"Patna",
    "Branch":"ME"
}
print("-----keys()------")
print(student1.keys())
# The line `for i in student1.keys():` is iterating over the keys of the dictionary `student1`. In
# each iteration, the variable `i` will hold one key from the dictionary, and then `print(i,
# student1[i])` will print the key along with its corresponding value from the dictionary. This loop
# essentially prints each key-value pair in the dictionary `student1`.
for i in student1.keys():
    print(i, student1[i])

print("-----values()----")    
# The line `for i in student1.values():` is iterating over the values of the dictionary `student1`. In
# each iteration, the variable `i` will hold one value from the dictionary, and then `print(i)` will
# print that value. This loop essentially prints each value in the dictionary `student1`.
for i in student1.values():
    print(i)

print("------item()------")    
print(student1.items())

# The line `for x, y in student1.items():` is iterating over the key-value pairs in the dictionary
# `student1`. In each iteration, the variables `x` and `y` will hold one key-value pair from the
# dictionary, and then `print(x, y)` will print the key along with its corresponding value. This loop
# essentially prints each key-value pair in the dictionary `student1`.
for x,y in student1.items():
    print(x,y)
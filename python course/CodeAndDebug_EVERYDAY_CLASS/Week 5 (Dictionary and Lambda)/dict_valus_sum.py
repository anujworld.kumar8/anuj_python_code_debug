subject = {
    "math":89,
    "physics":76,
    "science":67,
    "Hindi":77,
    "social-science":68
}

# `sub_sum =
# subject["math"]+subject["physics"]+subject["science"]+subject["Hindi"]+subject["social-science"]` is
# calculating the sum of the marks in the "math", "physics", "science", "Hindi", and "social-science"
# subjects from the `subject` dictionary. It is adding the marks obtained in each subject to get the
# total sum of marks across these subjects.
sub_sum = subject["math"]+subject["physics"]+subject["science"]+subject["Hindi"]+subject["social-science"]
print("sum of all the subject are: ",sub_sum)

# The line `for marks in subject.values():` is iterating over the values in the `subject` dictionary.
# It retrieves each value (marks) from the dictionary and executes the indented block of code for each
# value. In this case, it is summing up all the values in the dictionary to calculate the total sum of
# marks.
sum_marks=0
for marks in subject.values():
    sum_marks = sum_marks+marks
print("sum by 2nd way : ",sum_marks)

# The line `print("3rd way to sum ",sum(subject.values()))` is calculating the sum of all the values
# in the `subject` dictionary using the `sum()` function. It is a concise way to calculate the sum of
# all the values in the dictionary without using a loop or explicitly adding each value.
print("3rd way to sum ",sum(subject.values()))


# `print(list(subject.values()))` is printing a list of all the values in the `subject` dictionary. It
# retrieves all the values (marks) from the dictionary and converts them into a list, which is then
# printed to the console. This line of code will output a list containing the marks obtained in each
# subject in the dictionary.
print(list(subject.values()))
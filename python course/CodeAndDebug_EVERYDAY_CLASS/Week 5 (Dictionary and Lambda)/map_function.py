def even_odd(n:int):
    if n % 2 ==0:
        return n+1
    else:
        return n-1
    
my_list = [82,6,79,71,25,5,8,4,42]
x= list(map(even_odd, my_list))
print(x)
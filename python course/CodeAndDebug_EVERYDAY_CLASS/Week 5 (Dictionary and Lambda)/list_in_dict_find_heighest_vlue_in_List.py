# find the highest marks of each student (highest values from each list) and also who has highest total marks (sum of all the
# values and which is highest marks).
student_details = {
    "Aman" : [82,96,45,67,78],
    "Ram" : [48,78,25,46,34],
    "Mohan" : [78,45,96,50,89],
    "shiva" : [78,65,98,45,68]
}
# first way BEST WAY
for item, value in student_details.items():
    print(f"Name: {item}, highest marks: {max(value)}, Total marks: {sum(value)}")
    
print("====================================")
#2nd way using loops  
student_names= ""  
for itm , vlu in student_details.items():
    total_marks = 0
    max_mar=0   
    for i in vlu:
        total_marks = total_marks+i
        student_names = itm
        if i>= max_mar :
            max_mar=i    
    print(f"Name: {student_names}, highest marks: {max_mar}, Total marks: {total_marks}")    


  
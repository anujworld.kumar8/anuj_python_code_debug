# find the total highest marks and that student name.

student_details = {
    "Aman" : [82,96,45,67,78],
    "Ram" : [88,78,95,96,84],
    "Mohan" : [48,95,66,92,79]
}

total_marks = 0
highest_marks_student = ""
for item, value in student_details.items():
   if sum(value) >=total_marks:
        total_marks =  sum(value)
        highest_marks_student = item
print(highest_marks_student, total_marks)

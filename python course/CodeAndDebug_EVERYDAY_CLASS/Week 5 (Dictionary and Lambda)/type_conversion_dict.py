subject = {
    "math":89,
    "physics":76,
    "science":67,
    "Hindi":77,
    "social-science":68
}


# `x=str(subject)` is converting the dictionary `subject` into a string representation and storing it
# in the variable `x`. This allows you to manipulate the dictionary as a string, such as accessing
# individual characters as shown in the subsequent `print` statements.
x=str(subject)
print(x[0])
print(x[1])
print(x[2])
print(x[3])

# `y = list(subject)` is converting the dictionary `subject` into a list of its keys and storing it in
# the variable `y`. This operation creates a list containing all the keys present in the dictionary
# `subject`.
y = list(subject)
# `z = list(subject.keys())` is creating a list `z` that contains all the keys present in the
# dictionary `subject`. This operation extracts all the keys from the dictionary using the `keys()`
# method and stores them in a list. This allows you to access and manipulate the keys separately from
# the values in the dictionary.
z = list(subject.keys())
# `v = list(subject.values())` is creating a list `v` that contains all the values present in the
# dictionary `subject`. This operation extracts all the values from the dictionary and stores them in
# a list, allowing you to access and manipulate the values separately from the keys.
v = list(subject.values())
print("converting to list \n ",z)
print("converting to list \n ",y)
print("converting to list \n ",v)

# `a = tuple(subject)` is converting the dictionary `subject` into a tuple and storing it in the
# variable `a`. This operation creates a tuple containing all the key-value pairs present in the
# dictionary `subject`. Each key-value pair is represented as a tuple within the larger tuple. This
# allows you to access and manipulate the key-value pairs as a single unit in the form of tuples.
a = tuple(subject)
print("converting to tuple: \n",a)
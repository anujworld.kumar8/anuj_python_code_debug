details = {"name": "anuj",
           "age": 28,
           "location":"Patna",
           "gender": "Male"
           }

print(details["name"])
print(details["age"])
print(details["location"])

print(details["0"]) 
# The line `print(details["0"])` is trying to access the value associated with the key "0" in the
# `details` dictionary. However, there is no key "0" in the dictionary, so this will result in a
# KeyError. To fix this, you should either remove that line or make sure to access keys that actually
# exist in the dictionary.




# This line of code is taking input from the user in the form of three values separated by spaces. The
# `input("Enter the 3 values: ")` part prompts the user to enter three values. The `split()` method
# then splits the input string into a list of substrings based on spaces.
a,b,c = map(int, input("Enter the 3 values: ").split())

print(a , type(a))
print(b , type(b))
print(c , type(c))
print(a+b+c)

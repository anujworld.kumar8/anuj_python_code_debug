# This line of code is creating a lambda function called `add` that takes three arguments `a`, `b`,
# and `c`, and returns their sum. In this case, when you call `add(10, 20, 52)`, it will return the
# result of adding these three numbers together, which is 82.
add = lambda a,b,c: a+b+c
print('add: ', add(10,20,52))

is_even = lambda num: num%2 == 0
print('is_even: ', is_even(44))
print('is_even: ', is_even(11))

create_list_in_range = lambda start, end : [i for i in range(start, end+1)]
print('print_number_in_range: ', create_list_in_range(5,25))

get_list_of_even_number_from_range = lambda n1, n2 : [i for i in range(n1, n2+1) if i%2==0]
print('get_list_of_even_number_from_range: ', get_list_of_even_number_from_range(2,25))

is_palindrome = lambda num: num == num[::-1]
print('is_palindrome: ', is_palindrome(num=[12,63,12]))
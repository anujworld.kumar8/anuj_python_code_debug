student_details = { 
    "anirudh":{ 
        "age": 66, 
        "gender": "Male", 
        "address": "Surat", 
        "phone": 100,
        "history": 89,
        "Hindi": 88,
        "Maths": 64,
        "Science": 71,
        "Social-science": 66
                },
    "nihar":
        { "phone": 3213421, 
        "gender": "Male",
        "address": "Delhi", 
        "age": 16,
        "history": 81,
        "Hindi": 88,
        "Maths": 54,
        "Science": 41,
        "Social-science": 56
            },
        }

for name,details in student_details.items():
    total_marks = 0
    # for key, value in details.items():
    total_marks = (details['history'] 
                   + details["Hindi"] 
                   + details["Maths"] 
                   + details["Science"] 
                   + details["Social-science"])
    # or access bu using get() method
    total_marks2=(details.get("history",0) 
                  + details.get("Hindi",0) 
                  + details.get("Maths",0)
                  + details.get("Science",0)
                  +details.get("Social-science",0))
    # The line `print(name, total_marks)` is printing the total marks obtained by each student in the
    # `student_details` dictionary. It calculates the total marks by summing the marks obtained in the
    # subjects "history", "Hindi", "Maths", "Science", and "Social-science" for each student. The
    # total marks are then displayed along with the student's name. If a subject is not found in the dictionary
    print(name, total_marks)
    # The line `print(name, total_marks2)` is printing the total marks obtained by the student named
    # `name` based on the subjects "history", "Hindi", "Maths", "Science", and "Social-science". The
    # total marks are calculated using the `get()` method to retrieve the marks for each subject from
    # the `details` dictionary. If a subject is not found in the dictionary, it defaults to 0.
    print(name, total_marks2)
        

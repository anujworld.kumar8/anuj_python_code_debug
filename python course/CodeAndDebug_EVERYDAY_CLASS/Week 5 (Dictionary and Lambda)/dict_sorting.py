# sort a dictionary based on both keys and values.
anuj= { 
        "History": 89,
        "Hindi": 88,
        "Maths": 74,
        "Science": 79,
        "Social-science": 66
                }
# sorting dictionary on the basis of key
my_keys = list(anuj.values()) 
my_keys.sort()

print('my_keys: ', my_keys)
sd = {i: anuj[i] for i in anuj }
print('sd: ', sd)

# sorting dictionary on the basis of values.
print("Dictionary", anuj)
# Sorting key-value pairs by value, and by key if values are the same
sorted_items = dict(sorted(anuj.items(), key=lambda kv: (kv[1])))

print(sorted_items)

# sorting dictionary on the basis of key.
print("Dictionary", anuj)
# The line `sorted_items = dict(sorted(anuj.items(), key=lambda kv: (kv[0])))` is sorting the
# dictionary `anuj` based on its keys in ascending order.
sorted_items = dict(sorted(anuj.items(), key=lambda kv: (kv[0])))

print(sorted_items)

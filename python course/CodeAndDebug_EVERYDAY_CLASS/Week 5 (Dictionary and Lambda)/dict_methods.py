subjects = {
    "Mathematics": 85,
    "Science": 90,
    "English": 78,
    "History": 88,
    "Computer Science": 92
}
# subjects.clear()
print(subjects)

my_dict1 = {}
a= ["a","b","c"]
b=[1,2,3]
# Creating dictionary using for loop where list a contains all the key and list b contains all the values
for i in range(len(a)):
    my_dict1[a[i]]= b[i]
print(my_dict1)

# creates a dictionary with keys from list a, all set to the value 0. by using fromkeys()
my_dict2 = dict.fromkeys(a,0)
print(my_dict2)

print("English Marks : ",subjects["English"]) #Raises a KeyError if the key does not exist in the dictionary.
print("English Marks : ",subjects.get("English"))  #Returns None (or a specified default value) if the key is not found, instead of raising an error.




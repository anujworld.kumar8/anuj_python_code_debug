subjects = {
    "Mathematics": 85,
    "Science": 90,
    "English": 78,
    "History": 88,
    "Computer Science": 92
}
# user_input = input("Enter the subject name : ")
# result = subjects.get(user_input)
# if result:
#     print(result)
# else:
#     print(f"Subject not found: '{user_input}'")
    
# # 2nd way 
# if result != None:
#     print(result)
# else:
#     print(f"Subject not found: '{user_input}'")
    
# # 3rd way using membership operator in, not in BEST WAY
# if user_input in subjects:
#     print(subjects[user_input])
# else:
#     print(f"Subject not found: {user_input}")
    

# `subjects.pop("History")` is removing the key-value pair associated with the key "History" from the
# `subjects` dictionary. In this case, it will remove the entry for the subject "History" along with
# its corresponding score from the dictionary.
popped_element = subjects.pop("History")
print("return popped value: ",popped_element) 
print("After popped : ",subjects)

# UPdate dictionary
# Normal way
subjects["English"] = 74
subjects["Mathematics"] = 87
print(subjects)

# Update using update method
# The line `subjects.update({"Mathematics":100, "English":100, "Hindi": 100, "civics":89})` is
# updating the `subjects` dictionary by adding new key-value pairs and updating existing ones.
subjects.update({"Mathematics":100, "English":100, "Hindi": 100, "civics":89})
print(subjects)
details = {"name": "anuj", "age": 28, "location":"Patna"}
b = {"gender": "male"}


print(details+b)
# The code `print(details+b)` is trying to concatenate two dictionaries `details` and `b`. However,
# you cannot directly concatenate dictionaries in Python using the `+` operator. This will result in a
# `TypeError`.

print(details*3)
# `print(details*3)` is trying to multiply the dictionary `details` by 3. However, you cannot directly
# multiply a dictionary in Python using the `*` operator. This will result in a `TypeError`.


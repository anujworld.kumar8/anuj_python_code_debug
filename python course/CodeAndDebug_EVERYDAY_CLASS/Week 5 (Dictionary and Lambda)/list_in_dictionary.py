
#list contains the students 5 subjects marks, find the sum of all the marks 
student_details = {
    "Aman" : [82,96,45,67,78],
    "Ram" : [48,78,25,46,34],
    "Mohan" : [78,45,96,12,89]
}
# 1st way BEST way
for x,y in student_details.items():
    print(f"{x} scored total marks : {sum(y)} ")
    
# 2nd way using for loop
for a,b in student_details.items():
    total=0
    for i in b:
        total = total+i
    print(a,total)
        
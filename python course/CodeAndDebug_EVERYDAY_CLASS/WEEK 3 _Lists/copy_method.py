import copy

lst = [1, 2, 3]
new_lst = lst.copy()
print(lst)
print(new_lst) 

print(id(lst))
print(id(new_lst))

print(id(lst[0]))
print(id(new_lst[0]))

original = [1,2,3,7,6,9]
deep_copied = copy.deepcopy(original)

# Modifying the deep copy
deep_copied[0] = 10

print(original)        # Output: [[1, 2], [3, 4]] (original remains unchanged)
print(deep_copied)
print(id(original))
print(id(deep_copied))
print(id(original[0]))
print(id(deep_copied[0]))


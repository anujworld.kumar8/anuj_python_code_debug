# Membership OPerator (IN / NOT IN)
my_list = [8,5,4,5,8,6,3,1,44,8715,5,1,512,2,5,145]

count_value=my_list.count(56) #count() method used to count the value occurrence
print(count_value)

# if count_value > 0:
#     print("yes")
# else:
#     print("no")

if 5 in my_list:
    print("yes")
else:
    print("no")
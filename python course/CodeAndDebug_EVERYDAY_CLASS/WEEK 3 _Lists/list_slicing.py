# List slicing in Python allows you to access a range of elements from a list by specifying a start, stop, and optional step index.
# syntax list[start : stop : steps]
lst = [0, 1, 2, 3, 4, 5,6,7,8,9]
print("elements from index 1 to 4 ", lst[1:4] )   # Output: [1, 2, 3] (elements from index 1 to 4)
print("from given index to end ", lst[0:] )
print("from 0 index to list size index ", lst[0:len(lst)] )
print("first 3 elements ", lst[:3] )    # Output: [0, 1, 2] (first 3 elements)
print("elements from index 3 to the end ", lst[3:] )    # Output: [3, 4, 5, 6, 7, 8, 9] (elements from index 3 to the end)

print("every second element ", lst[::2] )   # Output: [0, 2, 4, 6, 8] (every second element)
print("start end and interval", lst[1:8:3] ) #[1, 4, 7]

print("last 3 elements ", lst[-3:] )   # Output: [7, 8, 9] (last 3 elements)
print("reverse the list ", lst[::-1] )  # Output: [9, 8, 7, 6, 5, 4, 3, 2, 1, 0] (reverse the list)

print("empty list ",lst[6:2])

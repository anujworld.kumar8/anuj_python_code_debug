def writeIntoFile(user_input, fileName):
    with open(fileName + ".txt", "w") as file:
        file.write(user_input)


user_input = input("Please enter your input: ")
fileName = input("Enter the file name without extention: ")
writeIntoFile(user_input, fileName)

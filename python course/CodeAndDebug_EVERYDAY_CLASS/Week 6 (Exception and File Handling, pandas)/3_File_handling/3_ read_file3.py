# count how many line are there in that (hello.txt) text file
def count_lines(filename):
    file = open(filename, "r")
    read = file.readlines()
    total_lines = len(read)
    print('total_lines: ', total_lines)
    file.close()
    
filename = "hello.txt"
count_lines(filename)
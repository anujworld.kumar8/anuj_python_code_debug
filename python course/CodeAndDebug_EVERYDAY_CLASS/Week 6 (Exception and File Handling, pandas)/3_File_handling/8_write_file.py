"""In the context of the open() function in Python, "w" stands for write mode. 
It is used to open a file for writing.
-If the file does not exist, it will be created.
-If the file already exists, its content will be overwritten (erased).
-Allows you to write data to the file but not read from it."""

# with open("write_file.txt", "w") as file:
#     file.write("My name is anuj kumar.")

"""In this code, the file is opened in append mode ("a"):

"a": Opens the file for appending data.
If the file does not exist, it creates a new one.
If the file exists, it adds new content to the end without overwriting the existing data."""


with open("write_file.txt", "a") as file:
    file.write("My name is anuj kumar.")

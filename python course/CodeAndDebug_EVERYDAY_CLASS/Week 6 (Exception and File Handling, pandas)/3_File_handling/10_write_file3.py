def writeIntoFile(user_input, fileName):
    with open(fileName + ".txt", "w") as file:
        file.write(user_input)


fileName = input("Enter your file name without extention: ")
u_input = ""
while True:
    user_input = input("Enter your input: ")

    if user_input.lower() == "q":
        break
    u_input = u_input + user_input
    u_input = u_input + "\n"

writeIntoFile(u_input[:-1], fileName)

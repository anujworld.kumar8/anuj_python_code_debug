my_set = {100,3,5,23," ", "Ram", "Ram", True, True}
print('my_set: ', my_set)
# it is iterable
for i in my_set:
    print(i, end=" ")
    
print("the data type is : ",type(my_set))
    
# slicing is not allowed in set
# print(my_set[:2])
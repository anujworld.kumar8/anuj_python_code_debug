a = {1,32,4,5,6,7,87,6}
b = {1,2,3,4,5,6,7,9}

'''In Python, the operators +, -, *, and / are not directly supported for set operations like union, difference, 
intersection, or division. If you try to run the code as written, it will raise errors'''

# print(a+b) 
# Python does not support + for sets. 
'''Solution: To get the union of two sets (all unique elements in both a and b), use a | b or a.union(b).'''

print(a-b) #This line will print the difference between a and b.The elements in a but not in b are {32, 87}, so the output is {32, 87}

# print(a*b)
# The * operator is not supported for sets. Multiplication does not apply to sets.
'''Solution: If you want the intersection (common elements in both a and b), use a & b or a.intersection(b).'''

# print(a/b)
# The / operator is not supported for sets. Division does not apply to sets.
'''Solution: There is no direct equivalent for division in set operations. However, you might mean symmetric difference (elements in either a or b but not in both), 
which can be achieved with a ^ b or a.symmetric_difference(b)'''


# Union
print("Union: ",a | b)  # Output: {1, 2, 3, 4, 5, 6, 7, 9, 32, 87}

# Intersection
print("Intersection: ",a & b)  # Output: {1, 4, 5, 6, 7}

# Symmetric Difference
print("Symmetric Difference: ",a ^ b)  # Output: {2, 3, 9, 32, 87}
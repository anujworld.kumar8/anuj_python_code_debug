my_dict = {"name":"Anuj", "place": "Bangalore", "id": "EST5695", "age": 27}

user_input = input("Enter the key: ")
try:
    if user_input not in my_dict:
        print(my_dict[user_input])
except KeyError:
    print(f"{user_input} key is not present")  
except:
    print("some other error")  

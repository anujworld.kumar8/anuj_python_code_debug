# Separate out int and float value in two different list.
# original_list = [2, 3.75, 0.04, 59.354, 6, 7.7777, 8, 9] 
# only_int=[....]  
# only_float=[....]
'''
NOT:
        To check data type we use type() function

        a = 10.04
        if type(a)== int:
            print("YES")
        else:
            print("NO")
'''

original_list = [2, 3.75, 0.04, 59.354, 6, 7.7777, 8, 9] 
only_int=[i for i in original_list if type(i)==int]  
only_float=[i for i in original_list if type(i)==float]

print(only_int)
print(only_float)



# list out all the prime number between 1 to 50 using a slicing
def prime(n: int)-> bool:
    count =0
    for i in range(1, n+1):
        if n%i ==0:
            count+=1
    if count==2 :   
        return True
    else:
        return False 
# print(prime(170))        
prime_lst = [i for i in range(1,50) if prime(i)==True]
print(prime_lst)
    

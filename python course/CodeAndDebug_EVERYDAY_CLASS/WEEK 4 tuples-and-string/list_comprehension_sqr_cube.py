# create a list if the number is odd make it square and if the number is even make it cube.
# input [1,2,3,4,5,6,7]  output: 1,8,9,64, 25, 216, 49

my_list = [i**3 if i%2 ==0  else i**2 for i in range(1,10)]
print(my_list)
#Take a range of 1 to 50 and make a list of those numbers which is divisible by 2 and 3 using list comprehension
lst = [i for i in range(1, 50) if i%2==0 and i%3 ==0 ]
# print(lst)

# ================2nd way using function============
def check_div(n):
    # for i in range(1, 50):
    if n %2==0 and n % 3==0:
        return True
    else:
        return False
        
my_list = [i for i in range(1, 50) if check_div(i)==True]
print(my_list)
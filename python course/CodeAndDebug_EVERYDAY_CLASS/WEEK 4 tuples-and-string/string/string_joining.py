# Joining
# join() used to convert list into string
# join only works with string
my_string = ["Python", "is", "a", "good", "Programming", "language"]
result = " ".join(i for i in my_string)
print(result)

my_list = [6,9,7,9,2,10,6]
result2 = " ".join(str(i) for i in my_list) # convert the int to str because join only work with string , it only works if list contains all the string value only
print(result2)
# String slicing
a = "python is good"
print(a[0:3])  #output: pyt
print(a[::-1])#output: doog si nohtyp
print(a[::3])#output: ph  o
print(a[-5:])#output:  good

my_string = "Pythonlanguage123@"
count=0
for i in my_string:
    if ord(i)>=97 and ord(i)<=122:
        count+=1
print("Alphabetes ",count)
        

# a = my_string.lower()
# my_string = my_string.upper()
# my_string = my_string.title()
# my_string = my_string.capitalize()
# my_string = my_string.swapcase()
# print(my_string.islower())
# print(my_string.isupper())
print("isalnum -> ",my_string.isalnum()) #check the string should only contains alphabets or number 
print("isalpha -> ",my_string.isalpha())# check sting should only contains alphabets  
print("isdigit -> ",my_string.isdigit()) # check only digits

casefold = my_string.casefold()
print("casefold- ",casefold)
print("strip ",my_string.strip('@')) #strip() method to remove the @ character from the beginning and end of the string. However, if the @ character is in the middle of the string (like in your example "Pythonlanguage@123"), the strip() method will not affect it.


# print(my_string)
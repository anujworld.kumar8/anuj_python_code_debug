my_string = "Python is a programming language"
# BY INDEX
for i in range(0, len(my_string)):
    print(my_string[i])
    
# BY VALUE
for i in my_string:
    print(i)
    
for index, value in enumerate(my_string):
    print(index, value)
'''Ask the character input from user until user enter 'quit' and return all the character by combing it as a single string
INPUT
ENTER CHARACTER: a
ENTER CHARACTER: 3
ENTER CHARACTER: 5
ENTER CHARACTER: h
ENTER CHARACTER: u
ENTER CHARACTER: quit

OUTPUT:
a35hu

'''
my_str = ""
while True:
    char = input("Enter character: ")
    if char == 'quit':
        break
    my_str +=char

print(my_str)
    
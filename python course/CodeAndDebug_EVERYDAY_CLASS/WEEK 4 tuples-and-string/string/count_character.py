a = "Python is good programming language"
count=0
for i in a:
    if i =='o' or i =="O":
        count+=1
    
print(count)

# Using ASCII value

a = "python is Ooa cool coding language"
count = 0
for ch in a:
    if ord(ch) == 111 or ord(ch) == 79:
        count += 1
print(count)
string  = "1234"
x = int(string)
print(x,type(x))

# string2 = "1256ab"
# # y = int(string2)  #this will throw an value error as string2 contains character value which can't be conversable to in
# # print(y, type(y))

string3 = "python programming language"
temp_list = list(string3)
print(temp_list)

r= "".join(i for i in temp_list)
print("r ", r)
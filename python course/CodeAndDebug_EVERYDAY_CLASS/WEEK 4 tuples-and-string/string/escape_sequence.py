print("Hello\nWorld!")   # Output: 
                         # Hello
                         # World!

print("Column1\tColumn2") # Output: Column1    Column2
print("She said, \"Python is awesome!\"")  # Output: She said, "Python is awesome!"
print("This is a backslash: \\")  # Output: This is a backslash: \
    
# INPUT : # py\\"a'"\'
print('py\\\\"a\'"\\\'')


r"""
    Escape Sequence	  |     Description
    ------------------|--------------------
    \'	              |     Single quote
    \"	              |     Double quote
    \\	              |     Backslash
    \n	              |     Newline (line break)
    \t	              |     Horizontal tab
    \r	              |     Carriage return
    \b	              |     Backspace
    \f	              |     Form feed
    \v	              |     Vertical tab
    \0	              |     Null character
    \N{name}	      |     Unicode character with the given name
    \uXXXX	          |     Unicode character with 16-bit hex value
    \UXXXXXXXX	      |     Unicode character with 32-bit hex value
    \xXX	          |     Character with hex value

"""
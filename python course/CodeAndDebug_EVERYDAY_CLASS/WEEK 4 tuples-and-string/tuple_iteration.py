my_tuple = (2,8,9,6,5,6,9,4,6,9,1,32,85,1,56)
# BY VALUE
for i in my_tuple:
    print(i, end=",")
print()
# BY INDEX
for i in range(0, len(my_tuple)):
    print(my_tuple[i], end=",")

print()    
for index, value in enumerate(my_tuple):
    print(index, value)

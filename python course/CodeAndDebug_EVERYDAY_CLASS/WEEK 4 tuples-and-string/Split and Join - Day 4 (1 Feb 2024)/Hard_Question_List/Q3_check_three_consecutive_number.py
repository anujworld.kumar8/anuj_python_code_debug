# Q3. Write a python program to check if the list contains three consecutive common numbers in Python.
'''
Input: [4, 5, 5, 5, 3, 81] 
Output: 5 
Input: [1, 1, 1, 64, 23, 64, 22, 22, 22]
Output: 1, 22 
Input: [154,1,4,56,67,879,22]
Output: No
'''
def check_three_consecutive_number(my_list: list):
    consecutive_numbers = []
    
    # Loop through the list and check for three consecutive numbers
    i = 0
    while i < len(my_list) - 2:
        if my_list[i] == my_list[i+1] == my_list[i+2]:
            consecutive_numbers.append(my_list[i])
            i += 3  # Skip the next two numbers as we already confirmed this group
        else:
            i += 1  # Move to the next number
    
    # Check if any consecutive numbers were found
    if consecutive_numbers:
        return ', '.join(map(str, consecutive_numbers))  # Return as a comma-separated string
    else:
        return "No"

# Example inputs
my_list1 = [4, 5, 5, 5, 3, 81] 
my_list2 = [1, 1, 1, 64, 23, 64, 22, 22, 22]
my_list3 = [154, 1, 4, 56, 67, 879, 22]

# Output the results
print(check_three_consecutive_number(my_list1))  # Output: 5
print(check_three_consecutive_number(my_list2))  # Output: 1, 22
print(check_three_consecutive_number(my_list3))  # Output: No

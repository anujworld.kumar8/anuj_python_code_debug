# Q1. Write a Python code to check if a given list is sorted in ascending order.
# You don't have to change the list. Just output YES if list is sorted else NO.

def check_sorted_list(my_list: list):
    check = "YES"
    for i in range(len(my_list)-1):
        
        if my_list[i]> my_list[i+1]:
            check= "NO"
            break
    print(check)
    
my_list = [4,6,896,12,78]
check_sorted_list(my_list)
# Q2. Given two sorted lists, write a Python code to merge them into a single sorted list.
# Don't use SORT() method. NOTE: user input must be sorted list.
def merge_two_list(list1, list2):
    i,j=0,0
    merged_list=[]
    while i< len(list1) and j < len(list2):
        if list1[i]< list2[j]:
            merged_list.append(list1[i])
            i+=1
        else:
            merged_list.append(list2[j])
            j+=1
    
    while i<len(list1):
        merged_list.append(list1[i])
        i+=1
    while j <len(list2):
        merged_list.append(list2[j])
        j+=1
    return merged_list
    
list1= [4,78,79,88,90]
list2= [81,84,1025,1542]
print(merge_two_list(list1, list2))

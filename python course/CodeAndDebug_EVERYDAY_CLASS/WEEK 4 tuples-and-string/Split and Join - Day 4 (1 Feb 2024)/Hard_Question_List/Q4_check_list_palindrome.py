# Q4. Write a Python code to check if a list is a palindrome (reads the same forwards and backwards).
# Just print "YES" or "NO"

# # LONG method using for loop and creating another reversed list
# def check_list_palindrome(my_List):
#     reversed_list = []
#     for i in range(len(my_List)-1,-1,-1):
#         reversed_list.append(my_List[i])
#     if my_List == reversed_list:
#         print("YES")
      
#     else:
#         print("NO")
   
# my_list = [1,2,1,1,2,1,789,5]
# check_list_palindrome(my_list)

# short method using list slicing
def check_list_palindrome(my_list):
    if my_list == my_list[::-1]:
        print("YES")
    else:
        print("NO")

# Example input
my_list = [1, 2, 1, 1, 2, 1]
check_list_palindrome(my_list)

        
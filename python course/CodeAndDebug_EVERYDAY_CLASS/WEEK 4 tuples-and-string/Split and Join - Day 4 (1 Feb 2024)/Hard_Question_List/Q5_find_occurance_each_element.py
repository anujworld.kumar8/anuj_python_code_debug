'''
Q5. Write a Python code to find the occurrence of each element in a list and print the element with the highest occurrence.
my_list = [54,14,11,12,54,14,14,54,11,54,11,11,11,5]
Output
54: 4 times
14: 3 times
11: 5 times
12: 1 times
The element with the highest occurrence is: 11 (5 times)
'''
# # METHOD-1 : using count() within a loop is less efficient because count()
# # iterates through the entire list for each element,
# def find_occurrence_each_element(my_list: list):
#     count =0
#     unique_list = set(my_list)
#     for i in unique_list:
#         count= my_list.count(i)
    
#         print(f"{i} : {count} times")

# my_list = [54,14,11,12,54,14,14,54,11,54,11,11,11,11,11]
# find_occurrence_each_element(my_list)

# METHOD-2 : using a dictionary to keep track of the count of each element in the list. After counting the occurrences.

def find_occurrence_each_element(my_list: list):
    # Create a dictionary to store occurrences of each element
    occurrence_dict = {}
    # Count occurrences of each element
    for element in my_list:
        if element in occurrence_dict:
            occurrence_dict[element] += 1
        else:
            occurrence_dict[element] = 1
    # Print occurrences of each element
    for key, value in occurrence_dict.items():
        print(f"{key}: {value} times")
    # Find the element with the highest occurrence
    max_element = max(occurrence_dict, key=occurrence_dict.get)
    print(f"The element with the highest occurrence is: {max_element} ({occurrence_dict[max_element]} times)")

# Example list
my_list = [54, 14, 11, 12, 54, 14, 14, 54, 11, 54, 11, 11, 11]
find_occurrence_each_element(my_list)
# Q7. Write a Python code to find the second largest element in a list without sorting.
def second_largest(numbers):
    if len(numbers) < 2:
        return None

    first, second = numbers[0], numbers[1]
    if first < second:
        first, second = second, first

    for n in numbers[2:]:
        if n > first:
            first, second = n, first
        elif n > second:
            second = n

    return second
my_list = [7,8,2,6,9,7,4,2,69,8,4,56,56,2,2]
print(second_largest(my_list))

# Q6. Write a Python code to find the difference between two lists (i.e., elements present in the first list but not in the second or
# element present in second list but not present in first ). 
def check_difference(list1, list2):
    # Create lists to hold unique elements
    unique_in_list1 = []
    unique_in_list2 = []

    # Check for elements in list1 that are not in list2
    for element in list1:
        if element not in list2:
            unique_in_list1.append(element)

    # Check for elements in list2 that are not in list1
    for element in list2:
        if element not in list1:
            unique_in_list2.append(element)

    return unique_in_list1, unique_in_list2  # Return both difference lists

# Example lists
mylist1 = [4, 6, 0, 5,45]
mylist2 = [87,4, 6, 0, 9]

# Find the differences
unique_in_list1, unique_in_list2 = check_difference(mylist1, mylist2)

# Print the results
if unique_in_list1:
    print("Elements present in the first list but not in the second:", unique_in_list1)
else:
    print("No elements present in the first list that are not in the second.")

if unique_in_list2:
    print("Elements present in the second list but not in the first:", unique_in_list2)
else:
    print("No elements present in the second list that are not in the first.")


# it is manly used to create a new list
a= [i for i in range(5)]

sqr = [i**2 for i in range(1,5)]

even_odd = ["Even" if i %2 == 0  else "Odd" for i in range(10)]
even_number = [i for i in range(1,11) if i%2==0]
reverse_num = [i for i in range(10,0,-1)]
print(even_odd)
print(even_number)
print(reverse_num)
print(sqr)
print(a)
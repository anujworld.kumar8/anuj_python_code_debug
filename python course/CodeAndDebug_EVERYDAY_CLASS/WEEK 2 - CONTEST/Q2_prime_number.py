# Q2. Create a function named as checkPrime that takes an integer as an argument. Print YES if the number passed is a prime number else print NO.
def checkPrime(num):
    if num <= 1:
        print("NO")
        return
    for i in range(2, int(num**0.5) + 1):
        if num % i == 0:
            print("NO")
            return
    print("YES")


num = int(input("Enter the number: "))
checkPrime(num)

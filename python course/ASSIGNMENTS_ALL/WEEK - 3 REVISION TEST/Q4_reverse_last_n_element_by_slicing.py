# Q4. Make your own list. Write a Python program that takes an integer as an input, and make a new list containing 
# the last n elements of the original list but in reverse order. Using slicing logic.

my_list = [5,9,3,2,6,45,6]
new_list = []
user_input = int(input("Enter the integer value : "))
new_list.extend(my_list[(len(my_list)- user_input) : ])
new_list.reverse()
print(new_list)

# Q5. Write a python program to interchange first and last elements in a list. 

my_list = [7,3,9,4,6,8,66,5,33]

temp = my_list[0]
my_list[0] = my_list[len(my_list)-1]
my_list[len(my_list)-1] = temp

print(my_list)


# ======2nd way======================

def swap(my_list):
    my_list[0], my_list[-1] = my_list[-1], my_list[0]
    return my_list

my_list = [7,3,9,4,6,8,66,5,33]

print(swap(my_list))

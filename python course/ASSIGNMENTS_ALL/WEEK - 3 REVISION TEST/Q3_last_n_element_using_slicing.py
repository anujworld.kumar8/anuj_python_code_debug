# Q3. Make your own list. Write a Python program that takes an integer as an input,
# and make a new list containing the last n elements of the original list. Using slicing logic.

my_list = [8,3,6,4,5,2,5,8]
user_input = int(input("Enter the number"))
new_list = []
new_list.extend(my_list[(len(my_list)-user_input):])
print(new_list)
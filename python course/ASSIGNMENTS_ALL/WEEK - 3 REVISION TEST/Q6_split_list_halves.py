# Q6. Write a Python code to split a list into two halves using list slicing. (Keep the length of list even).
my_lst = [1,9,3,54,6,9,78,6,5]

first_halves = []
second_halves = []

first_halves.extend(my_lst[0: (len(my_lst)//2)])
second_halves.extend(my_lst[(len(my_lst)//2):])
print(first_halves)
print(second_halves)
# Q2. Make your own list of numbers. Ask a start and end position from User.
# Make another different list which will contain number from start to end position. Use slicing logic. 

my_list = [4,3,6,9,7,5]
start =  int(input("Enter the start value: "))
end =  int(input("Enter the end value: "))

new_list = []
new_list.extend(my_list[start: end])

print(new_list)
'''Q10. Ask 10 numbers from the user and put them into the list. Now remove
all the even numbers from that list.'''
from typing import List

def find_even_number(my_list: List[int])-> List:
    even_list = []
    for i in my_list:
        if i %2 ==0:
            even_list.append(i)
            
    return even_list
            
my_list = []
for i in range(10):
   num = int(input(f"Enter the integer value at index {i} :")) 
   my_list.append(num)
print("Even Numbers are : ", find_even_number(my_list))
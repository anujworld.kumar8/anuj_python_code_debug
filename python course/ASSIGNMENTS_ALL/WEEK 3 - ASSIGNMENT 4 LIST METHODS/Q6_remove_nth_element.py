# Q6. Write a program to remove the nth index element from a list using a function.
'''
lst = [34, 11, 91, 59, 33, 22]
removeNth(lst,3)
# Output
[34, 11, 91, 33, 22]

lst = [34, 11, 91, 59, 33, 22]
removeNth(lst,67)
# Output
# (Do not throw error instead 
# display this if index does not exists
Index does not exists 

'''
def remove_nth_elemet(nth, lst):
    # for i in lst:
    #     print(lst[nth])
    if nth >= len(lst):
        print(f"your list size is {len(lst)}, your index value should be less than list size")
    else:
        lst.pop(nth)
        print(lst)
        
lst = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
nth = int (input("Enter the number of element you want to remove "))
remove_nth_elemet(nth, lst)
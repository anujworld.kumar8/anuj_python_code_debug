# Q11. Write a python program which prints all the values whose count is greater than 3. (Make sure to make a list with at least 15 numbers)

lst = [7, 82, 69, 32385, 8, 14, 10, 85, 69, 32,7,82,10,82,7,7]
count_lst = []
for i in set(lst): #Converts the list to a set, which ensures that each unique value is processed only once.
   count_value=  lst.count(i)
   if count_value >=3:
       count_lst.append(i)
    
print(count_lst)
       
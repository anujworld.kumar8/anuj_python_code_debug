"""
Write a python program which prints all the 
values whose count(occurance) is greater than 3. 
(Make sure to make a list with at least 15 numbers)
"""
from typing import List
def count_occurance( my_list: List [int]  )-> list:
    counted_number=[]
    for i in my_list:
        count_num= my_list.count(i)
        if count_num> 3:
            if i not in counted_number:
                counted_number.append(i)
            
    return counted_number
    
my_list = [7,6,9,4,3,6,5,2,3,6,5,2,3,6,9,7,4,1,1,2,5,8,4,1,2,7,4,8,7]
print(count_occurance(my_list))


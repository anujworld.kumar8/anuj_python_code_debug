# Q6. Write a program to remove the nth index element from a list using a
# function
def remove_nth_index_element(lst1, num):
    if num < 0 or num > len(lst1):
        print(f"Error: Index {num} is out of range for the list.")
        # return lst1
    else:
        removed_element = lst1.pop(num)
        print(f"Element at index {num} ({removed_element}) has been removed.")
        return lst1
       

lst1 = [4, 89, 6, 9, 3, 6, 4, 44, 11, 12, 45, 91]
num = int(input("Enter the index to remove (0 to {}): ".format(len(lst1)-1)))

print("Original list:", lst1)
modified_list = remove_nth_index_element(lst1, num)
if modified_list:
    print("Modified list:", modified_list)



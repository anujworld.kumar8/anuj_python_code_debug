'''
Q7. Make two lists of same length and pass it to a function. Return a third
list where each element is the sum of index.
'''
from typing import List
def sum_of_each_index(list1: List[int], list2: List[int]) ->List[int]:
    list3 = []
    sum=0
    if len(list1) != len(list2):
        print("Both lists must be of the same length.")
    else:
        for i in range(0, len(list1)):
            sum = list1[i] + list2[i]
            list3.append(sum)
        print(list3)
        
list1 = [4,6,9,1,2]
list2 = [2,7,2,1,2]
sum_of_each_index(list1, list2)
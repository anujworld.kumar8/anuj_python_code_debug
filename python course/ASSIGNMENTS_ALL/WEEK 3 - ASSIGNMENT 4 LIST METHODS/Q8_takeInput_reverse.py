# Q8. Take 10 integer inputs from user and store them in a list. Now, copy all
# the elements in another list but in reverse order.

from typing import List
def reverse(list1: List[int]) -> List[int]:
    list2=[]
    
    for i in range(len(list1)-1, -1 ,-1):
        list2.append(list1[i])
    return list2   

    
list1 = []
for i in range(10):
    num= int (input(f"Enter the element at index {i} : "))
    list1.append(num)
    
print(reverse(list1))

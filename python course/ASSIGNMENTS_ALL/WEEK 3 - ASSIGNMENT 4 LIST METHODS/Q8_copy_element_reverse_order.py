# Q8. Take 10 integer inputs from user and store them in a list. Now, copy all the elements in another list but in reverse order.
def copy_element(lst):
    # lst.reverse() #using reverse function
    reversed_lst = []
    
    for i in range(len(lst)-1 ,-1, -1):
        reversed_lst.append(lst[i])
        
    return reversed_lst
    
lst = []

print("Enter the element")
while len(lst)!=10:
    value=int(input())
    lst.append(value)
# lst = [1,2,3,6,5,6,8,9]
print(copy_element(lst))
    
# Q9. Make a list. Write a simple program for addition of the 2nd element from start and 2nd element from the end.

lst = [1,8,9,8]
if len(lst)<2:
     print("Cannot add 2nd and last 2nd element as not enough elements in list")
else:    
    sum_second_element = lst[1] + lst[-2]
    print(sum_second_element)
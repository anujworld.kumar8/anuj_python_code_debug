'''
Q1. Write a Python function to find the Maximum and minimum of three numbers. Use 3 parameters. Make 2 different functions named as maxi and mini. 
Q2. Attempt the same leap year question (Week 1 - Assignment 2 - Q8) but using function. Try calling function with different years as a parameter and check the output. 
Q3. Attempt the same bill calculator question (Week 1 - Assignment 2 - Q5) but using function. Try calling function with different bill amount as a parameter and check the output. 
Q4. Attempt Week 1 - Assignment 2 (Q6) and Week 1 - Assignment 2 (Q7) using function.
'''
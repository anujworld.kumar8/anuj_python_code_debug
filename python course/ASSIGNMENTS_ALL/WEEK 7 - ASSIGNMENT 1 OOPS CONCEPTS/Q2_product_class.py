"""Q2. Design a class called Product with properties like name, price, category, and quantity. 
Implement a constructor to initialize these attributes. 
Add methods to calculate the total price after applying a discount, ask discount percent within that method. 
"""


class Product:
    def __init__(self) -> None:
        self.name = input("Enter the name : ")
        self.price = float(input("Enter the price of the product: "))
        self.category = input("Enter the category: ")
        self.quantity = int(input("Enter the quantity: "))

    def calculate_the_total_price(self):
        discount = float(input("Enter the discoint percent : "))
        total_price = self.price * self.quantity
        final_price = total_price - (total_price * (discount / 100))

        print(f"==>> total_price: {total_price}")
        print(f"==>> final_price: {final_price}")


p1 = Product()
p1.calculate_the_total_price()

"""Q3. Employee Class with Performance Evaluation 
Attributes:
name: Name of the employee. 
age: Age of the employee. 
gender: Gender of the employee.
phone: Phone number of the employee. 
salary: Salary of that employee
Methods: __init__(self, name, age, gender, phone,salary): 
Constructor method to initialize the employee object with name, age, gender, and phone number, salary attributes. 
change_salary(self): Method that asks within the new salary and updates the salary of that employee 
display_details(self): Method to display all details of the employee, including name, age, gender, phone number, salary.
"""


class Employee:
    def __init__(self):
        self.name = input("Enter the name: ")
        self.age = int(input("Enter the age: "))
        self.phone = int(input("Enter the phone number: "))
        self.salary = float(input("Enter the employee slary: "))

    def change_salary(self):
        new_salary = float(input("Enter the updated salary : "))
        self.salary = new_salary

    def display_details(self):
        print(f"Employee name : {self.name}")
        print(f"Employee age : {self.age}")
        print(f"Employee phone : {self.phone}")
        print(f"Employee salary : {self.salary}")


emp = Employee()
emp.change_salary()
emp.display_details()

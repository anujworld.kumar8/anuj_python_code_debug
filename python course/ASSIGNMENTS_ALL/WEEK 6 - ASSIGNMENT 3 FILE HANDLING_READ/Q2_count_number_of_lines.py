def count_lines(filename):
    try:    
        f = open(filename, "r")
        r= f.readlines()
        # METHOD 1st
        '''
        count_l = 0
        for i in r:
            count_l +=1
        f.close()
        return count_l'''
        
        #METHOD 2nd
        f.close()
        return len(r)
        
    except FileNotFoundError:
        print(f"File not found {file_name}")
        return -1  # Return -1 to show user there is an error

file_name = "demoFile.txt"
print(count_lines(file_name))
'''Q9. There is a file having numbers in each line of that file. Calculate the
total of all numbers.
Example (File Content)
100
250
100
50
98
OUTPUT
598'''

def calculate_total(filename:str):
    try:
        file = open(filename, "r")
        data = file.read()
        total_sum = 0
        numbers = data.split()
        for i in numbers:
            total_sum += int(i)
        file.close()
        
        return total_sum
    except FileNotFoundError:
        print(f"file not found {filename}")
    except ValueError:
        print(f"invalid value found {i} in '{filename}'")
        return -1

file_name = "amount_file.txt"
totalSum =  calculate_total(file_name)
if totalSum != -1:
    print(totalSum)
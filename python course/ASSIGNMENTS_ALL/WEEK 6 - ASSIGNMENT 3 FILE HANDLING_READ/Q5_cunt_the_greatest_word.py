'''Q5. Make a function which takes filename as a parameter. Print the words in that file which has length greater than 4'''

def count_greatest_word(filename:str ):
    try:
        file = open(filename, "r")
        data = file.read()
        words = data.split()
        count_word = 0
        for word in words:
            if len(word) >4:
                print(word,end=", ")
        file.close()
    
    except FileNotFoundError:
        print(f"File not found {filename}")
        return -1
filename = "demoFile.txt"    
count_greatest_word(filename)

    
            
            





'''Q6. Make a function which takes filename as a parameter. Return the count
of words which end with e.
Example (File Content)
python is great
this is a great course
we are doing dsa
OUTPUT
2
'''
def count_word_end_with_e(filename:str)->int:
    try:
        file = open(filename, "r")
        data = file.read()
        file.close()
        count = 0
        words = data.split()
        for word in words:
            if word.lower().endswith('e'):
                print(word,end=", ")
                count +=1
        print()
        return count
    except FileNotFoundError:
        print(f"File not found {filename}")
        return -1
        
file_name = "demoFile.txt"
count_words = count_word_end_with_e(file_name)
if count_words !=-1:
    print(f"Total number words end with 'e' are {count_words}")
    
            
    

'''Q7. Make a function which takes filename as a parameter. Return the count of uppercase characters in that file.'''
def count_uppercase(filename : str):
    try:
        file = open(filename, "r")
        data = file.read()
        count = 0
        for i in data:
            word = i.isupper()
            if word:
                count +=1
                print(i, end=",")
        print()
        return count
    except FileNotFoundError:
        print("file not found ")
        return -1
file_name = "demoFile.txt"
count = count_uppercase(file_name)
if count != -1:
    print(f"Count of uppercase characters = {count}")
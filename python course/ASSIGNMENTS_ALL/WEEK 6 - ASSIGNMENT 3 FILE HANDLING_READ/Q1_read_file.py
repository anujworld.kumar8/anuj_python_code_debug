'''Q1. Make a function which takes filename as a parameter. Return the number of words present in that file. '''
def count_words(filename):
    f = open(filename, "r")
    r= f.read()
    
    words_count = r.split()
    return len(words_count)
    
filename = "demoFile.txt"
print(count_words(filename))
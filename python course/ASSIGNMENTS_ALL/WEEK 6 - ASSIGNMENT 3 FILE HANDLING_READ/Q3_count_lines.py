"""
Make a function which takes filename as a parameter. 
Return the number of lines present in that file which does not start with T or t.
"""

def count_lines(filename):
    try:
        file = open(filename, "r")
        data = file.readlines()
        count = 0
        for i in data:
            if i.strip() and i[0] !="T" and i[0]!="t":
                count+=1
        file.close()
        return count
    except FileNotFoundError:
        print(f"file not found: {filename}")
        return -1
        
filename = "demoFile.txt"
line_count = count_lines(filename)
if line_count != -1:
    print(f"Number of lines in {filename} not starting with 'T' or 't' = {line_count}")
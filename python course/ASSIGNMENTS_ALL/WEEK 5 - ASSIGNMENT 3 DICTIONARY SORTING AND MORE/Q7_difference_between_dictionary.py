'''Q7. Create a Python program to find the difference between two dictionaries.
First dictionary: {'a': 1,'b': 2,'c': 3}
Second dictionary: {'b': 2,'c': 3,'d': 5}
OUTPUT:
Keys present only in the first dictionary: ['a']
Keys present only in the second dictionary: ['d']
Keys present in both dictionaries: ['b','c']
'''
from typing import Dict

def findDifference(dictionary1, dictionary2) -> None:
    present_in_dict1 = []
    present_in_both = []
    for k in dictionary1:
        if k not in dictionary2:
            present_in_dict1.append(k)
        else:
            present_in_both.append(k)

    present_in_dict2 = []
    for k in dictionary2:
        if k not in dictionary1:
            present_in_dict2.append(k)

    print(f"Keys present only in the first dictionary: {present_in_dict1}")
    print(f"Keys present only in the second dictionary: {present_in_dict2}")
    print(f"Keys present in both dictionaries: {present_in_both}")


findDifference({"a": 1, "b": 2, "c": 3}, {"b": 2, "c": 4, "d": 5})

print("=============2nd way to finds the differences between the two dictionaries ===========")
# 2nd way using list comprehension to finds the differences between the two dictionaries 
def find_difference_between_dict(first_dict: Dict, second_dict: Dict):
    first = [i for i in first_dict.keys()]
    second = [j for j in second_dict.keys()]
    present_only_in_first = [i for i in first if i not in second]
    print('Keys present only in the first dictionary: ', present_only_in_first)
    
    present_only_in_second = [i for  i in second if i not in first]
    print('Keys present only in the second dictionary: ', present_only_in_second)
    
    present_both = [i for i in first if i in second]
    print('Keys present in both dictionaries: ', present_both)
    
first_dict = {'a': 1,'b': 2,'c': 3}
second_dict = {'b': 2,'c': 4,'d': 5}
find_difference_between_dict(first_dict, second_dict)

'''Q5. Create a Python function to sort a dictionary by its values. And return
that new dictionary.'''

def sort_dict(my_dict:dict):
    sorted_dict = dict(sorted(my_dict.items(), key=lambda k: k[1]))
    return sorted_dict
    
    
my_dict = {"M1" : 409, "M2" : 96, "M3" : 89}
print('sort_dict: ', sort_dict(my_dict))


'''Q3. Write a Python program to print a dictionary line by line.
Sample Output
Dict = { "Sam" : {"M1" : 89, "M2" : 56, "M3" : 89},
"Suresh" : {"M1" : 49, "M2" : 96, "M3" : 89} }
Sam
M1 : 89
M2 : 56
M3 : 89
Suresh
M1 : 49
M2 : 96
M3 : 89
'''
My_Dict = { "Sam" : {"M1" : 89, "M2" : 56, "M3" : 89},
"Suresh" : {"M1" : 49, "M2" : 96, "M3" : 89} }

for key, value in My_Dict.items():
    print(key)
    
    for key1, value1 in value.items():
        print(key1, value1)
        
    
'''Q4. Write a Python program to Convert two lists into a dictionary
Sample Output
keys = ["One", "Two", "Three", "Four", "Five"]
values = [1, 2, 3, 4, 5]
Convert Two List to Dict = {'One' : 1, 'Two' : 2, 'Three' : 3, 'Four' : 4, 'Five' : 5}'''

keys = ["One", "Two", "Three", "Four", "Five"]
values = [1, 2, 3, 4, 5]

my_dict = {}
for i in range(len(keys)):
    
    my_dict[keys[i]] = values[i]

print(my_dict)


# Using zip to combine keys and values into a dictionary
my_dict = dict(zip(keys, values))

print("Convert Two List to Dict =", my_dict)
'''Q11. Write a Python program to sort a dictionary by the length of its keys
Original dictionary: {'apple': 2, 'banana': 3, 'pear': 4, 'orange': 5}
Sorted dictionary by key length:
{'pear': 4, 'apple': 2, 'banana': 3, 'orange': 5}
'''
def sort_dict_on_key_length(my_dict: dict):
    sorted_dict = dict(sorted(my_dict.items(), key=lambda k: len(k[0])))
    print(sorted_dict) 
    

my_dict = {'apple': 2, 'banana': 3, 'pear': 9, 'orange': 5}
sort_dict_on_key_length(my_dict)    
'''Q6. Write a Python program to find the maximum and minimum value in a dictionary.
'''
from typing import Dict


def maximum(dictionary: Dict) -> int | float:
    max_num = float("-inf")  # Most negative number
    for v in dictionary.values():
        if v > max_num:
            max_num = v
    return max_num


def minimum(dictionary: Dict) -> int | float:
    min_num = float("inf")  # Most positive number
    for v in dictionary.values():
        if v < min_num:
            
            min_num = v
    return min_num

my_dict = {"Anirudh": 54.6, "Akul": 12, "Nihar": 35.123, "Sanjay": 111}

mini = minimum(my_dict)
maxi = maximum(my_dict)

print(f"Minimum = {mini}")
print(f"Maximum = {maxi}")

# 2nd way using max and min method
my_dict2 = {"M1" : 409, "M2" : 96, "M3" : 89}
 
max_value = max(my_dict2.values())
print('max_value: ', max_value)
min_value = min(my_dict2.values())
print('min_value: ', min_value)


'''Q8. Create a Python function to reverse a dictionary (swap keys and values). Make sure the values are different.
Original dictionary: {'a': 1, 'b': 2, 'c': 3}
Reversed dictionary:
{1: 'a', 2: 'b', 3: 'c'}'''

# 1st way to swap using dictionary comprehension
def swap_dict(my_dict: dict):
    swapped_dict = {value:key for key,value in my_dict.items()}
    
    print(swapped_dict)
    
my_dict = {'a': 1, 'b': 2, 'c': 3}
swap_dict(my_dict)

# 2nd way to swap just using for loop
new_swapped_dict ={}
for key, value in my_dict.items():
    new_swapped_dict[value] = key
print('new_swapped_dict: ', new_swapped_dict)
    
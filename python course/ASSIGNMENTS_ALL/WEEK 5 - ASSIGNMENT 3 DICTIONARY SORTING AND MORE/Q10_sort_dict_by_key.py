'''
Q10. Write a Python program to sort a dictionary by its keys in ascending
order.
Original dictionary: {'b': 2, 'a': 1, 'c': 3}
Sorted dictionary by keys:
{'a': 1, 'b': 2, 'c': 3}
'''

my_dict = {'b': 2, 'a': 1, 'c': 3}
# 1st way

# The line `print(dict(sorted(my_dict.items())))` is sorting the dictionary `my_dict` by its keys in
# ascending order and then converting the sorted key-value pairs back into a dictionary. Finally, it
# prints the sorted dictionary.
print(dict(sorted(my_dict.items())))


# 2nd way

# The line `sorted_dict = dict(sorted(my_dict.items(), key=lambda k: k[0]))` is sorting the dictionary
# `my_dict` by its keys in ascending order.
sorted_dict = dict(sorted(my_dict.items(), key=lambda k: k[0]))
print(sorted_dict)






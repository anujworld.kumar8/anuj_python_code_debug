'''
Q1. Write a Python script to sort (ascending and descending) a dictionary
by value.
Sample Output
dictionary = {1: 2, 3: 4, 4: 3, 2: 1, 0: 0}
Ascending order = { 0:0, 2:1, 1: 2, 3: 4}
Descending order = {3: 4, 4: 3, 1: 2, 2: 1, 0: 0}
'''

def sort_dict_asc_and_desc(my_dict: dict):
    asc_dict = dict(sorted(my_dict.items(), key=lambda kv:kv[1]))
    desc_dict = dict(sorted(my_dict.items(), key=lambda xy:xy[1], reverse=True))
    print('asc_dict: ', asc_dict)
    print('desc_dict: ', desc_dict)
 
my_dict = {1: 2, 3: 4, 4: 3, 2: 1, 0: 0}
sort_dict_asc_and_desc(my_dict)
"""Q1. Create a function named oddCharacters which takes a string as a parameter. 
Now return a list of characters which appears odd times in that string.
input: hello
Output: ['h','e','o']
"""


def oddCharacters(my_str: str):
    # Dictionary to count occurrences of each character
    char_count = {}
    for char in my_str:
        char_count[char] = char_count.get(char, 0) + 1

    # Create a list of characters with odd occurrences
    odd_chars = [char for char, count in char_count.items() if count % 2 != 0]

    return odd_chars


# Input from the user
my_str = input("Enter the string value: ")
result = oddCharacters(my_str)
print("Characters appearing odd times:", result)

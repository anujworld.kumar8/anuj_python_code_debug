"""Q4. Convert Snake case to Pascal case. 
Input: python_is_great 
Output: PythonIsGreat 
Input: we_are_learning_python_programming 
Output: WeAreLearningPythonProgramming 

"""

# def convert_snake_to_pascal(user_input: str):
#     splited_str = user_input.split("_")
#     updated_str = ""
#     for i in splited_str:
#         updated_str += i.capitalize()
#     return updated_str

# OR


# def convert_snake_to_pascal(user_input: str):
#     split_str = user_input.split("_")
#     updated_str = "".join(words.capitalize() for words in split_str)
#     return updated_str

# or


def convert_snake_to_pascal(user_input: str):
    res = user_input.replace("_", " ").title().replace(" ", "")
    return res


"""Replaces all underscores (_) with spaces ( ).
.title() -> Converts the first letter of each word to uppercase and the rest to lowercase."""

user_input = "we_are_learning_python_programming"
print(convert_snake_to_pascal(user_input))

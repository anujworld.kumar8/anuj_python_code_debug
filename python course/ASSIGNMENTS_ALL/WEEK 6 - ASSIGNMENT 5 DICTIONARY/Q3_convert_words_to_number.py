"""Q3. Given a string S, containing numeric words, the task is to convert the given string to the actual number. 
Input: S = “zero four zero one” 
Output: 0401 
Input: S = “four zero one four” 
Output: 4014 
"""

# def wordsToNumber(word_number, user_input):
#     result = ""
#     user_input_words = user_input.split()
#     for num in user_input_words:
#         result = result + str(word_number.get(num))

#     return result

# OR


def wordsToNumber(word_number, user_input):
    return "".join(str(word_number.get(num)) for num in user_input.split())


word_number = {
    "zero": 0,
    "one": 1,
    "two": 2,
    "three": 3,
    "four": 4,
    "five": 5,
    "six": 6,
    "seven": 7,
    "eight": 8,
    "nine": 9,
}
user_input = "zero four zero one"
print(wordsToNumber(word_number, user_input))

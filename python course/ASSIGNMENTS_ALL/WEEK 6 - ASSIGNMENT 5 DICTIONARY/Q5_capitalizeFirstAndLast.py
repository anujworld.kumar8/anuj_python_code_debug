"""Q5. Write a Python program to capitalize the first and last letters of each word in a given string. 
Input: python is a great language 
Output: PythoN ExerciseS PracticE SolutioN 
Input: delhi is best city with 0 AQI 
Output: DelhI IS BesT CitY WitH 0 AqI"""

# def capitalizeFirstAndLastWord(user_str: str) -> str:
#     splitted_str = user_str.split()
#     result = []
#     for word in splitted_str:
#         if len(word) > 1:
#             new_str = word[0].upper() + word[1:-1] + word[-1].upper()
#         else:
#             new_str = word.upper()

#         result.append(new_str)
#     return " ".join(result)


# or
def capitalizeFirstAndLastWord(user_str: str) -> str:

    user_str = result = user_str.title()
    result = ""
    for word in user_str.split():
        result += word[:-1] + word[-1].upper() + " "
    return result


# Example usage
user_str = "python is a great language"
result = capitalizeFirstAndLastWord(user_str)
print(result)

"""Q7. Convert a list of Tuples into Dictionary 
Input: [("akash", 10), ("gaurav", 12), ("anand", 14), ("suraj", 20), ("akhil", 25), ("ashish", 30)] 
Output: {'akash': [10], 'gaurav': [12], 'anand': [14], 'ashish': [30], 'akhil': [25], 'suraj': [20]} 
Input: [('A', 1), ('B', 2), ('C', 3)] 
Output: {'B': [2], 'A': [1], 'C': [3]}"""


def convert_tuple_to_dict(my_list_tuple: list):
    my_dict = {}

    for obj in my_list_tuple:
        my_dict[obj[0]] = [obj[1]]

    return my_dict


my_list_tuple = [
    ("akash", 10),
    ("gaurav", 12),
    ("anand", 14),
    ("suraj", 20),
    ("akhil", 25),
    ("ashish", 30, 20),
]
print(convert_tuple_to_dict(my_list_tuple))

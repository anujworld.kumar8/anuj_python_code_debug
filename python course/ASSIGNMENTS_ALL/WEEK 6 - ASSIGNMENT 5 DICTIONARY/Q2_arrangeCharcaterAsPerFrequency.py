"""Q2. Create a function named arrangeChars which takes a string as a parameter. 
Now return a string with max frequency chars at start. 
Input: "aaeroplane"
Output: "aaaeeropln"
"""


def arrangeChars(my_str: str):
    # Dictionary to count occurrences of each character
    char_count = {}
    for char in my_str:  # Iterate over the string, not char_count
        char_count[char] = char_count.get(char, 0) + 1

    # Sort the dictionary by values in descending order (max frequency first)
    characters_count = dict(
        sorted(char_count.items(), key=lambda x: x[1], reverse=True)
    )

    # Construct the result string
    result = ""
    for char, count in characters_count.items():
        result += char * count  # Append each character multiplied by its count

    return result


# Example input
user_input = "heelllllooo"
print(arrangeChars(user_input))

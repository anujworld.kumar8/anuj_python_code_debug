"""Q6. Write a Python program to generate two strings from a given string. For the first string, 
use the characters that occur only once, and for the second, use the characters that occur multiple times in the said string. 
Input: aabbccegh 
Output string1 = egh 
  string2 = abcf 
Input: heello 
Output string1 = ho 
  string2 = el 
"""

# def seprate_the_string(user_str: str):
#     count_letters = {}
#     one_time = ""
#     repeated_times = ""
#     for char in user_str:
#         count_letters[char] = count_letters.get(char, 0) + 1

#     for letter, count in count_letters.items():
#         if count >= 2:
#             repeated_times += letter
#         else:
#             one_time += letter
#     print(f"==>> one_time: {one_time}")
#     print(f"==>> repeated_times: {repeated_times}")


# user_string = "heello"
# seprate_the_string(user_string)

# or


def generateStrings(input_string):

    char_freq = {}
    for ch in input_string:
        char_freq[ch] = char_freq.get(ch, 0) + 1
    # Part 1 contains single occurrence characters
    part1 = [key for (key, count) in char_freq.items() if count == 1]

    # Part 2 contains multiple occurrence characters
    part2 = [key for (key, count) in char_freq.items() if count > 1]

    return part1, part2


input = "heello"

s1, s2 = generateStrings(input)

print("".join(s1))
print("".join(s2))

'''Q2. Write a program to rotate the characters in a string by a given number of positions. 
For example, given the input "abcdef"  
and rotation of 2, the output should be "efabcd". 
Ask string and rotation from the user. 
'''

def rotation_of_character(my_str:str, rotation: int):
    rotation = rotation % len(my_str)
    rotated_my_str = my_str[-rotation:] + my_str[:-rotation]
    
    print(rotated_my_str) 

my_str = "abcdefghijkl"
rotation = 5
rotation_of_character(my_str, rotation)
'''Q6. Write a Python program to convert string values of a given dictionary into integer/float data types. 
Example 1: Original list: [{'x': '10', 'y': '20', 'z': '30'}, {'p': '40', 'q': '50', 'r': '60'}] 
Output: [{'x': 10, 'y': 20, 'z': 30}, {'p': 40, 'q': 50, 'r': 60}] 
Example 2: Original list: [{'x': '10.12', 'y': '20.23', 'z': '30'}, {'p': '40.00', 'q': '50.19', 'r': '60.99'}] 
Output: [{'x': 10.12, 'y': 20.23, 'z': 30.0}, {'p': 40.0, 'q': 50.19, 'r': 60.99}]
'''

def convert_string_values(my_list: dict):
    for i in my_list:
        for k, v in i.items():
            if "." in v:
                i[k] = float(v)
            else:
                i[k] = int(v)
    return my_list
            
    
my_list = [{'x': '10', 'y': '20', 'z': '30'}, {'p': '40', 'q': '50', 'r': '60'}] 
print(convert_string_values(my_list))
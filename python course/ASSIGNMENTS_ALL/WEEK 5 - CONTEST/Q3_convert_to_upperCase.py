'''
Q3. Write a Python program to convert a given string to all uppercase if it contains at least 2 uppercase characters in the first 4 characters. 
Input: pyTHon 
Output: PYTHON 
Input: helLo 
Output: helLo 
Input: gOOD 
Output: GOOD
'''
def convert_to_upper_case(my_str: str):
    count = sum(1 for i in my_str[:4] if i.isupper())
    print('count: ', count)
    
    if count >=2:
        return my_str.upper()
    else:
        return my_str
my_str = "LLPYtHonSSS"
print(convert_to_upper_case(my_str))
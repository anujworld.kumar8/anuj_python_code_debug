'''Q1. Given a list of strings, concatenate them into a single string separated by spaces. For example given the 
input ["Hello", "World", "Python"],
the output should be  "Hello World Python". 
Make a list on your own. Don’t use the JOIN function. 
'''

def concatenateStringBySpace(my_str_list : list):
    for i in my_str_list:
        print(i, end=" ")

my_str_list = ["Hello", "World", "Python"]
concatenateStringBySpace(my_str_list)

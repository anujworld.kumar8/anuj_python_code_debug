'''Q4. Create a dictionary that counts the frequency of words in a given string. Ask string from user. 
Example 1 Input String: "The sun is shining and the weather is nice" 
Output: { 'The': 1, 'sun': 1, 'is': 2, 'shining': 1, 'and': 1, 'the': 1, 'weather': 1, 'nice': 1 } 
Example 2 Input String: “The cat and the dog played in the park The cat chased the dog” 
Output: { 'The': 2, 'cat': 2, 'and': 1, 'dog': 2, 'played': 1, 'in': 1, 'the': 3, 'park': 1, 'chased': 1 } 
'''
def count_word_frequency(my_str: str):
    my_dict = {}
    my_str = my_str.lower()
    splitted_str = my_str.split()
    for i in splitted_str:
        wordcount = my_str.count(i)
        my_dict[i] = wordcount
        
    return my_dict
    
my_str = "The cat and the dog played in the park The cat chased the dog"    
print(count_word_frequency(my_str))
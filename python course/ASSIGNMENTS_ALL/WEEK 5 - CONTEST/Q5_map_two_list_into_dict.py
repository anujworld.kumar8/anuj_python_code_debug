'''Q5. Write a Python program to map two lists into a dictionary. Everything in both lists should be unique.
 Example 1 list1 = ['red', 'green', 'blue'] 
 list2 = ['#FF0000','#008000', '#0000FF'] 
Output: {'red': '#FF0000', 'green': '#008000', 'blue': '#0000FF'} '''


# 1st normal way
def map_two_list_into_dict(list1: list, list2: list):
    mapped_dict = {}
    for i in range(len(list1)):
        mapped_dict[list1[i]] = list2[i]
    return mapped_dict

list1 = ['red', 'green', 'blue']    
list2 = ['#FF0000','#008000', '#0000FF'] 

# 2nd by zip function
zippedlist = dict(zip(list1, list2))
print(zippedlist)
print(map_two_list_into_dict(list1, list2))


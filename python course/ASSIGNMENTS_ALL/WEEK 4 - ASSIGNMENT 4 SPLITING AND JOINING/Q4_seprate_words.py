# Q4. Ask 5 integers from user. Then ask a single character from user. Print
# those integers separated by that character entered by user

def separate_by_character(user_list,user_ch):
    
    return user_ch.join(str(i) for i in user_list)
  
user_list = []
for i in range(1,6):
    num=int(input(f"Enter number {i} = "))
    user_list.append(num)
        
user_ch = input("Enter one character value")
print(separate_by_character(user_list,user_ch))



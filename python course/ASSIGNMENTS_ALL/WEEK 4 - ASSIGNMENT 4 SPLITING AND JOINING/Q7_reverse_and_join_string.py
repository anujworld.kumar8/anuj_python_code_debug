# Q7. Write a function which accepts a String and another string (which will be a single character) as a parameter. 
# Join that string along with that character but in reverse.

def reverse_and_join(my_str: str, ch: str):
    words = my_str.split()
    words.reverse()
    result = ch.join(i for i in words)
    return result

    # Single line
    # return char.join(i for i in string.split()[::-1])

my_str = "This is my str"
ch  = '*'
print(reverse_and_join(my_str, ch))
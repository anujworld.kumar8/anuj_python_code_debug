# Q6. Write a function which accepts a String and another string (which will be a single character)
# as a parameter. Join that string along with that character

def join_string_by_charcter(my_str: str, ch: str)-> str:
    splitted_string = my_str.split()
    result = ch.join(i for i in splitted_string)
    return result

my_str  = "This is my str"
character = '*'
print(join_string_by_charcter(my_str, character))
    
    
    
    
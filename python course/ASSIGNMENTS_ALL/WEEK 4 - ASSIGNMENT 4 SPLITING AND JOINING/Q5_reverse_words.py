# Q5. Write a function which accepts a String as a parameter and return each word being in reverse.
def reverse_words(my_str: str):
    splitted_list = my_str.split()
    
    result = " ".join(i[::-1] for i in splitted_list)
    return result
                
    
my_str = "this is my string"
print(reverse_words(my_str))

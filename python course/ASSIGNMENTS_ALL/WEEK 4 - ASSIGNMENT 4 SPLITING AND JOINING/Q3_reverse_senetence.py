"""
Write a function which accepts a String as a parameter and return the reversed words as a String.
"""
def reverse_word(my_str: str):
    word = my_str.split()
    word.reverse()
    result = " ".join(i for i in word)
    return result
    
print(reverse_word("this is python"))
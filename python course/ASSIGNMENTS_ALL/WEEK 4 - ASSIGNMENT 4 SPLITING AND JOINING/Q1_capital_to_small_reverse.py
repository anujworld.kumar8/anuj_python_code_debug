# Q1. Ask a string from user. If the length of string is odd, then change all the capital letters to small and
# vice versa, but if the length of string is even, reverse the string. Do this using a function and return the output.

# LONG METHOD USING lower() and upper() methods
def changing_string(my_str: str) -> str:
    # Check if the length of the string is odd
    if len(my_str) % 2 != 0:
        # Swap the case of each character and store the result in a new string
        new_str = ""
        for ch in my_str:
            if ch.isupper():
                new_str += ch.lower()
            else:
                new_str += ch.upper()
        return new_str
    else:
        # Reverse the string if the length is even
        return my_str[::-1]

# SHORT METHOD using swap method

def convert_string(my_str1: str):
    if len(my_str1) %2 ==0:
        return my_str1[::-1]
    else:
        return my_str1.swapcase()
    

# Take user input
my_str = "Python is Good Programming Language." 
my_str1 = "python is object oriented programming klanguage"

# Print the result after processing
print("Using long method: ",changing_string(my_str))
print("Using long method: ",convert_string(my_str1))



"""
Write a function which accepts a String as a parameter and return the list of words.
"""
from typing import  List
def list_to_word(my_str: str)-> List[str]:
    return my_str.split()

my_str = "This is just a string"
print(list_to_word(my_str))
# Q1. Make a dictionary with keys as subject name (physics, chemistry, etc.) and values as their marks. Print the highest marks scored.
subject = {
    "physics":89,
    "chemistry":74,
    "math":68,
    "science":77,
    "Hindi":94
}
# with using max function
# `print("Highest marks : ",max(subject.values()))` is printing the highest marks scored among all
# the subjects in the dictionary `subject`. It uses the `max()` function to find the maximum value
# among the values in the dictionary and then prints it along with the message "Highest marks : ".
print("Highest marks : ",max(subject.values()))

# without using any function
# `print("Marks without using method: ", value)` is printing the highest marks scored among all the
# subjects in the dictionary `subject` without using any built-in function like `max()`. It achieves
# this by iterating through the values of the dictionary and comparing each value with the current
# highest value stored in the variable `value`. If a value is found that is greater than or equal to
# the current highest value, it updates the `value` variable to that value. Finally, it prints the
# highest marks stored in the `value` variable along with the message "Marks without using method: ".
value = 0
for marks in subject.values():
    if marks >= value:
        value= marks
print("Marks without using method: ",value)
    








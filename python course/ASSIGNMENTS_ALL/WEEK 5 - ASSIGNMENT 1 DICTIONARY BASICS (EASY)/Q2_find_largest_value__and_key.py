# Q2. Make a dictionary with keys as subject name (physics, chemistry, etc.) and values as their marks. 
# Print the name of the subject with highest marks scored. 
subject = {
    "physics":89,
    "chemistry":74,
    "math":68,
    "science":77,
    "Hindi":94
}

highest_marks = 0
highest_sub = ""
for key, value in subject.items():
    if value > highest_marks:
        highest_marks = value
        highest_sub = key

print(key, value)

# The line `print(key, value)` is attempting to print the last key and value pair that were iterated
# over in the for loop. However, there is a mistake in this line. It should be `print(highest_sub,
# highest_marks)` instead of `print(key, value)` to correctly print the subject with the highest marks
# scored.

    
    
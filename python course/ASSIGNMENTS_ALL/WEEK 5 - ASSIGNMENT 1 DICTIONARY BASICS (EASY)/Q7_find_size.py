# Q7. Python program to find the size of a Dictionary. Basically print how
# many total key-value pair are there
from typing import Dict
def count_key(my_dict:Dict)-> bool:
    count=0
    for i in my_dict.keys():
        count+=1
    print(count)    
        
    #OR   
    
def countKeys(dictionary: Dict) -> int:
    return len(dictionary.keys())
  
my_dict = {"name": "Anuj",
           "Age": 28,
           "Branch": "CSE",
           "JOb": "Software Developer"
           }
count_key(my_dict)
print(countKeys({}))
print(countKeys({"name": "xyz"}))
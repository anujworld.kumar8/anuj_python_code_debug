# Q3. Make a dictionary with keys as subject name (physics, chemistry, etc.)
# and values as their marks. Print the name of the subject which has marks more than passing marks (above 33). 

subject = {
    "physics":39,
    "chemistry":74,
    "math":28,
    "science":77,
    "Hindi":94
}
for sub, marks in subject.items():
    if marks >= 33:
        print(sub,marks)
    

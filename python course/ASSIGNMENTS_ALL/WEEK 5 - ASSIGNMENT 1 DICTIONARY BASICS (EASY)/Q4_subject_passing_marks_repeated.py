# Q3. Make a dictionary with keys as subject name (physics, chemistry, etc.)
# and values as their marks. Print the name of the subject which has marks more than passing marks (above 33). 

subject = {
    "physics":89,
    "maths": 72,
    "science":23,
    "hindi":68,
    "history":22
}

for sub, mark in tuple(subject.items()):
    if mark>=33:
        print(sub, mark)
    
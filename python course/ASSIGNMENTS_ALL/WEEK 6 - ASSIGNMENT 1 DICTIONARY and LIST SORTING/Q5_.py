'''Q5. Here's a student data. The first 3 elements are marks of student. Sort this list and print it. 
student_data -
[ [78, 92, 85, "Alice"], 
[82, 79, 81, "Bob"], 
[92, 88, 85, "Charlie"],
 [80, 79, 82, "Diana"], 
[92, 85, 90, "Eva"],
 [85, 80, 86, "Frank"],
 [87, 92, 88, "Gina"]
]

OUTPUT 
Diana has scored 241 
Bob has scored 242 
Frank has scored 251 
Alice has scored 255 
Charlie has scored 265 
Eva has scored 267 
Gina has scored 267
'''
student_data =[ [78, 92, 85, "Alice"], 
[82, 79, 81, "Bob"], 
[92, 88, 85, "Charlie"],
 [80, 79, 82, "Diana"], 
[92, 85, 90, "Eva"],
 [85, 80, 86, "Frank"],
 [87, 92, 88, "Gina"]
]
sorted_data = (sorted(student_data, key=lambda k: k[0]+k[1]+k[2]))
for detail in sorted_data:
    total_sum = detail[0]+ detail[1] + detail[2]
    name = detail[3]
    print(f"{name} has scored {total_sum}")
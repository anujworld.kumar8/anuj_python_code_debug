'''Q1. Here’s a students data and their marks. 
student_data = { "Alice": [85, 90, 88, 92, 89], 
"Bob": [78, 82, 79, 81, 80], "Charlie": [92, 95, 88, 85, 91],
 "Diana": [76, 80, 79, 82, 85],
 "Eva": [88, 92, 85, 90, 87],
 "Frank": [83, 85, 80, 86, 88],
 "Gina": [90, 87, 92, 88, 86], }
Display the name of student and total marks in ascending order. 
OUTPUT: 
Bob has scored 400 
Diana has scored 402 
Frank has scored 422 
Eva has scored 442
Gina has scored 443
Alice has scored 444 
Charlie has scored 451
'''
def total_marks_ascending(student_data: dict):
    sorted_total_marks_asc = dict(sorted(student_data.items(), key=lambda k: sum(k[1])))
    for name, marks in sorted_total_marks_asc.items():
        total_marks = sum(marks)
        print(f"{name} has scored {total_marks}")
        
    # # or
    # sorted_total_marks_asc = sorted([(name, sum(marks)) for name, marks in student_data.items()], key=lambda x: x[1])
    # for name, total_marks in sorted_total_marks_asc:
    #     print(f"{name} has scored {total_marks}")

# Student data
student_data = {
    "Alice": [85, 90, 88, 92, 89],
    "Bob": [78, 82, 79, 81, 80],
    "Charlie": [92, 95, 88, 85, 91],
    "Diana": [76, 80, 79, 82, 85],
    "Eva": [88, 92, 85, 90, 87],
    "Frank": [83, 85, 80, 86, 88],
    "Gina": [90, 87, 92, 88, 86]
}

# Call the function
total_marks_ascending(student_data)

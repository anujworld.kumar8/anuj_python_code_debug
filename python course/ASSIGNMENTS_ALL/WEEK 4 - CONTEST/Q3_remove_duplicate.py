# Q3. Python Program to remove all duplicates from a given string. 

def remove_duplicate_char(my_str: str):
    temp_str = ""
    for char in my_str:
        if char not in temp_str:
            temp_str+=char
    return temp_str
    
my_str = "This is my string"
print(remove_duplicate_char(my_str))
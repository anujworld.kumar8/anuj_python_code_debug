'''
Q1. Create a python function named is Palindrome which accepts a string as a parameter and return True if its a palindrome.
Palindrome are words which is same when read from start and same when read from the end.
'''
def palindrome(my_str:str):
    temp_str = my_str
    rev_str = temp_str[::-1]
    if rev_str == temp_str:
        return True
    else:
        return False
my_str = "ThhT"
print(palindrome(my_str)  )  
    
    
    

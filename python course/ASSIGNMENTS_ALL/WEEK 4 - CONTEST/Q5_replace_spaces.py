# Q5. Ask a string from user. Replace all the space characters with "-". Do not use replace() method.
def replace_space(my_str: str):
    splitted_char = my_str.split()
    result = "_".join(splitted_char)
    return result

my_str = "This is my string to replace all the space with underscore"
print(replace_space(my_str))
            
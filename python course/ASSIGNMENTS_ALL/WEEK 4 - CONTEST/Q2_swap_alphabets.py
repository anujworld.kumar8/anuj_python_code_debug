'''
Q2. Keep asking characters from user until he presses q on the keyboard. Change all the capital letters to small, 
and all the small letters to capital. 
(Don't use swapcase())
'''
user_all_input  = ""
i=1
while True:
    user_input = input(f"Enter {i} string character : ")
    i+=1
    if ord(user_input) >= 65 and ord(user_input)<=90:
        user_all_input +=chr(ord(user_input)+32)
    else:
        user_all_input +=chr(ord(user_input)-32)
    
    if user_input == 'q' or user_input == 'Q':
        break
    
# print(user_all_input.swapcase())
print(user_all_input)
    
'''
Q6. Make a password strength function. It will accept a string from the user. 
A Return True if it is a strong password. Strong password has these characteristics.
Minimum 8 character 
Minimum 1 uppercase alphabet 
Minimum 1 lowercase alphabet 
Contains at least 1 special symbol (any symbol) 
Minimum 1 digit
'''
def password_validation(my_passwords: str):
    has_length = False
    has_uppercase = False
    has_lowercase = False
    has_symbol = False
    has_digit = False
    if len(my_passwords)>=8:
        has_length = True
    for char in  my_passwords:
        if ord(char) >= 65 and ord(char) <=90: #  or if char.isupper():
            has_uppercase = True
        elif ord(char) >= 97 and ord(char) <=122: # or  if char.islower():
            has_lowercase = True
        elif ord(char) >= 48 and ord(char) <=57: #or if char.isdigit()
            has_digit =True
        elif not char.isalnum():
            has_symbol = True
    if has_length and has_uppercase and has_lowercase and has_symbol and has_digit:
        return True
    else:
        return False
   
user_input = input("Please enter your password: ")
print(password_validation(user_input))
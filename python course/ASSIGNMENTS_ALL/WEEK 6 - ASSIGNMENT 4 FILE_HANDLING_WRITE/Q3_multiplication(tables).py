"""Q3. Create a function named multiplication which takes 2 parameter which are filename and a number.
 Write the multiplication table of that number in that filenam Content in filename after running the code, lets suppose number = 5
5 x 1 = 5 
5 x 2 = 10 
5 x 3 = 15 
... 
... 
5 x 10 = 50
"""


def multiplication(num: int, filename: str):
    try:
        with open(filename + ".txt", "w") as file:
            for i in range(1, 11):
                file.write(f"{num} * {i} = {num*i} \n")
            print(f"Multiplication table of {num} has been written to '{filename}'.")
    except IOError as e:
        print(f"File error: {e}")
    except Exception as e:
        print(f"An unexpected error occurred: {e}")


file_name = input("Enter the file name wihout extention: ")
user_input = int(input("Enter a interger number: "))
multiplication(user_input, file_name)

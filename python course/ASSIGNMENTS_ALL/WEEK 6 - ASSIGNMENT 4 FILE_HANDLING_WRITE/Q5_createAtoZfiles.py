"""Q5. Write a Python program to generate 26 text files named A.txt, B.tx and so on up to Z.txt. 
There should be no content in any of them. You just need to create these files. 
(Search for how to create a file on google. you can use Write mode to create a file """

import os, string


def create_multiple_file():
    folder_name = "Q5Destination_folder"
    os.makedirs(folder_name, exist_ok=True)
    for names in string.ascii_letters:
        file_path = os.path.join(folder_name, f"{names}.txt")
        with open(file_path, "w") as file:
            file.write("")


create_multiple_file()

"""Q6. There is a file having sentences in it. Create a new file result.txt having the same sentence as it is 
but also number of vowels written beside each line. 
Original file (Content) 
python is great 
we are doing dsa 
HELLLLLOOO 

Output (result.txt) 
python is great 4 
we are doing dsa 6 
HELLLLLOOO 4 
"""

# def find_vowel(Source_file, destination_file):
#     try:
#         vowels = {"a", "e", "i", "o", "u"}
#         with open(Source_file, "r") as file1:
#             datas = file1.readlines()
#         with open(destination_file, "w") as file2:
#             for char in datas:
#                 count = 0

#                 for i in char:

#                     if i.lower() in vowels:
#                         count += 1
#                 file2.write(f"{char.strip()}  {count} \n")

#     except FileNotFoundError:
#         print("file not found")

# OR


def find_vowel(source_file, destination_file):
    try:
        vowels = {"a", "e", "i", "o", "u"}

        with open(source_file, "r") as file1:
            lines = file1.readlines()
        with open(destination_file, "w") as file2:
            for line in lines:
                count = sum(1 for char in line if char.lower() in vowels)
                file2.write(f"{line.strip()} {count} \n")

    except FileNotFoundError:
        print("file not found")


source_file = "Q6_Source_file.txt"
destination_file = "Q6_DestinationFile.txt"
find_vowel(source_file, destination_file)

# OR


# def countVowels(sentence: str) -> int:
#     vowels = "aeiouAEIOU"
#     count = 0
#     for char in sentence:
#         if char in vowels:
#             count += 1
#     return count


# def addVowelCount(input_file: str, output_file: str) -> None:
#     try:
#         with open(input_file, "r") as file_in:
#             lines = file_in.readlines()

#         with open(output_file, "w") as file_out:
#             for line in lines:
#                 line = line.strip()
#                 vowel_count = countVowels(line)
#                 file_out.write(f"{line} {vowel_count}\n")

#     except FileNotFoundError:
#         print("Error: Input file not found.")
#     except IOError:
#         print("Error: An I/O error occurred.")


# addVowelCount("input.txt", "result.txt")

"""Q2. There is a file named numbers.txt and the content is given below. Content (numbers.txt) 
34 67 98 11 -87 100
Create another file named numbers_result.txt. It should have the following
content in it based on numbers.txt.
34 Even
67 Odd
98 Even
11 Odd
-87 Odd
100 Even
"""


def findEvenOdd(source_file, destination_file):
    try:
        with open(source_file, "r") as file1:
            data = file1.read().split()
        with open(destination_file, "w") as file2:
            for num in data:
                number = int(num)
                if number % 2 == 0:
                    file2.write(f"{number}, Even \n")
                else:
                    file2.write(f"{number}, Odd \n")
    except FileNotFoundError:
        print("file not found")


source_file = "Q2_source_file.txt"
destination_file = "Q2_destination.txt"

findEvenOdd(source_file, destination_file)

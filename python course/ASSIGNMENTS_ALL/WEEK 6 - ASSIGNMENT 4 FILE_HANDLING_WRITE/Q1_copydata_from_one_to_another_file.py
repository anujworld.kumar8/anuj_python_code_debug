"""Q1. Make a function named copyFileContent which takes 2 filenames (filename1, filename2) 
as an argument. Copy the content of filename1 to filename2. """


def copyFileContent(filename1, filename2):
    try:
        with open(filename1, "r") as file1:
            data = file1.read()

        with open(filename2, "a") as file2:
            file2.write(data)
    except FileNotFoundError:
        print(f"Error: File not found {filename1, filename2}")
    except IOError:
        print("Error: An I\O error occured")


file_name1 = "Q1_filename10.txt"
file_name2 = "Q1_filename2.txt"
copyFileContent(file_name1, file_name2)

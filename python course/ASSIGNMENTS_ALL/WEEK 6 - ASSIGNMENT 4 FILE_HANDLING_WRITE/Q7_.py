"""Q7. There is a file (any random file) having sentences in it. Create a new result.txt having each sentence in a reverse order. 
Original file (Content) 
python is great 
we are doing dsa 
HELLLLLOOO 


Output (result.txt) 
HELLLLLOOO 
we are doing dsa 
python is great
"""


def reverse_and_store(source_file, destination_file):
    try:
        # Open the source file and read all lines
        with open(source_file, "r") as src_file:
            data = src_file.readlines()

        # Reverse the order of lines
        data.reverse()

        # Write the reversed lines to the destination file
        with open(destination_file, "w") as dest_file:
            for line in data:
                dest_file.write(
                    line.strip() + "\n"
                )  # Strip trailing whitespace and add newline

        print(f"Reversed lines have been stored in {destination_file}")
    except FileNotFoundError:
        print(f"Error: The file '{source_file}' was not found.")
    except Exception as e:
        print(f"An unexpected error occurred: {e}")


# User input for file names
source_file_name = input("Enter the source file name: ")
destination_file_name = "Q7_Destination_file.txt"

# Function call
reverse_and_store(source_file_name, destination_file_name)

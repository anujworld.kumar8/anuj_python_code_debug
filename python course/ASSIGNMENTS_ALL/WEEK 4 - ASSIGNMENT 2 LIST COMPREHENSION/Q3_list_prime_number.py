# Q3. Write a Python program to generate a list of prime numbers less than
# 500 using list comprehension.
# Example output: [2, 3, 5, 7, 11, 13, 17, 19, 23, ..., 491, 499]
def check_prime(n):
    count =0
    for i in range(1, n+1):
        if n%i ==0:
            count+=1
    if count==2:
        return True
    
def generateFactorialList(num):
    return [i for i in range(1, num) if i<num and check_prime(i)]

print(generateFactorialList(500))
    
            




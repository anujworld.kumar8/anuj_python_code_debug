# Q6. Remove duplicates from the list just by using list comprehension.

# my_list  = [4,6,3,4,5,2,1,7,8,9]
# new_list = []
# [new_list.append(i) for i in my_list if i not in new_list]
# print(new_list)

# ====BY FUNCTION=====

from typing import List
def generate_list(lst: List)->None:
    new_list = []
    [new_list.append(i)  for i in lst if i not in new_list]
    return new_list

my_list  = [4,6,3,4,5,2,1,7,8,9]
print(generate_list(my_list))



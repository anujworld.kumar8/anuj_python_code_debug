# Q8. Write a python program which prints all the values whose count is greater than 3.
# (Make sure to make a list with at least 15 numbers) Store them in another list using list comprehension

def count_num(n):
    count_list = []
    [count_list.append(i) for i in n if n.count(i)>=3 and i not in count_list]
    return count_list
    
lst = [4,5,2,4,5,2,4,5,6,3,9,9,9,9,9,2]
print(count_num(lst))
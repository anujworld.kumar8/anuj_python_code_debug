# Q4. Generate a list of numbers less than 1000 which are divisible by the sum of their digits. 
# (These were solved in contests)
def sum_digits(n):
    sum=0
    while(n!=0):
        r= n%10
        sum=sum+r
        n=n//10
    return sum

def generate_sum_digit_list(num):
    return [i for i in range(1, num) if i % sum_digits(i)==0]

print(generate_sum_digit_list(500))


# Q7. Make two lists of same length and pass it to a function. 
# Return a third list where each element is the sum of index. Use List Comprehension (Week 3 - Assignment 4, Q7) 


def sum(lst1, lst2):
    sum_lst= []
    [sum_lst.append(lst1[i]+ lst2[i]) for i in range(0, len(lst1))]
    return sum_lst
    
lst1 = [4,6,3,6,9,7,5]                                                                                                  
lst2 = [8,6,1,60,90,10,2]                                                                                                  

print(sum(lst1, lst2))


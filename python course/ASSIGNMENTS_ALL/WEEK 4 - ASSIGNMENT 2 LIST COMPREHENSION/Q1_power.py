# Q1. Write a Python program to generate a list of powers of 2 less than 100 using list comprehension. 
# Example output: [1, 2, 4, 8, 16, 32, 64] 100 can be changed to anything. 

"""
Write a Python program to generate a list of powers of 2 less
than 100 using list comprehension
"""

from typing import List
from math import sqrt


def generatePowerList(num: int) -> List[int]:
    # return [2**i for i in range(0, num) if 2**i < num]
    return [
        2**i for i in range(0, int(sqrt(num)) + 1) if 2**i < num
    ]  # More optimal


x = generatePowerList(100)
print(x)
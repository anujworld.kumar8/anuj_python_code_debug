# Q4. Write a Python program to reverse a tuple

def reverse_tuple(my_tuple):
    return [my_tuple[i] for i in range(len(my_tuple)-1, -1,-1)]
    
my_tuple = (4,8,2,3,6,9,7,56)
print(reverse_tuple(my_tuple))

# using inbuilt reverse function
my_tuple_1 = (40,80,20,30,60,90,70,56,45)

reverse_tuple2 = tuple(reversed(my_tuple_1))
print(reverse_tuple2)
    
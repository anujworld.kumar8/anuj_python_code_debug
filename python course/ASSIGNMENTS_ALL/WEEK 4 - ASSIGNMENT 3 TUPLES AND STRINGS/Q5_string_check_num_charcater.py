# Write a Python program to check if a string has at least one letter and
# one number. If it has at least one letter and one number then print YES else
# print NO.

def check_string_value(str_value):
    str_count=False
    num_count=False
    for i in str_value:
        if i.isalpha():
            str_count=True
        elif i.isdigit():
            num_count=True
    if str_count and num_count:    
        print("YES")
    else:
        print("NO")  

str_value="India is 3rd largest powerful country"            
check_string_value(str_value)
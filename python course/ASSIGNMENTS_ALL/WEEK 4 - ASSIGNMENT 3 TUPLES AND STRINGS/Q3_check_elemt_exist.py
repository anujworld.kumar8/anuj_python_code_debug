# Q3. Write a Python program to check whether an element exists within a
# tuple. Ask something from user, if that exists in tuple, then print “YES” else
# print “NO”

# 1st way
def check_element(my_tuple , num):
    if num in my_tuple:
        print("YES")
    else:
        print("NO")
        
my_tuple = (4,6,8,3,2,4,7,8,91)   
num = int (input("Enter the number"))
check_element(my_tuple, num)

def elementExistsInTuple(element, t):
    return element in t


# 2nd way

my_tuple = (1, 2, 3, 4, 5)

e = int(input("Enter an element = "))

if elementExistsInTuple(e, my_tuple):
    print("YES")
else:
    print("NO")
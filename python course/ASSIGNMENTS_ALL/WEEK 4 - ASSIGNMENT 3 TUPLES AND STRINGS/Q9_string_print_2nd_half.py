# Q9. Write a python program to only print second half of the string. Ask string from user

def print_2nd_half(my_str):
    str_2nd_half_length = len(my_str)//2
    print(my_str[str_2nd_half_length:])
    

my_str = input("Enter the string: ")
print_2nd_half(my_str)
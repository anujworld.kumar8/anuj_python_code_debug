# Write a python program to ask a string from user. Then count the
# number of vowels and number of consonants in that string. 
# (Make sure there are no spaces in string when you enter in terminal and also do
# not type any special characters except from alphabets)

def count_vowels_consonants(my_str):
    has_vowels = 0
    has_consonants = 0
    
    vowels = ['a','i','e','o','u','A', 'E', 'I', 'O', 'U']
    for i in my_str:
        if i in vowels:
            has_vowels += 1
        elif i.isalpha():
            has_consonants+=1
    print("Vowels = ",has_vowels)       
    print("Consonants = ", has_consonants)

user_input =  input("Please enter the string value: ")
count_vowels_consonants(user_input)        

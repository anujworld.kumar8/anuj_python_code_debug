# Q11. Ask a string from user. Print how many spaces are there in that string.
def count_str(my_str: str)->int:
    space_count=0
    for i in my_str:
        if ord(i) == 32:
            space_count+=1
    return space_count
            
my_str = input("Enter the string: ")
print(count_str(my_str))
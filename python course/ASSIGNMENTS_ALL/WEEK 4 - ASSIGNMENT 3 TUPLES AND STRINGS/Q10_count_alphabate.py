"""
Ask a string from user. Print total count of alphabets (a-z or A-Z) in that string.
"""
def count_alphabates(my_str: str)-> int:
    # # using isalpha function 
    # has_alphabates = 0
    # for i in my_str:
    #     if i.isalpha():
    #         has_alphabates+=1
    # print(has_alphabates)
    # Using iteration and ascii code
    
    has_alphabates = 0
    new_str= my_str.lower()
    for i in new_str:
        ascii_code = ord(i)
        if ascii_code>=97 and ascii_code<= 122:  
            has_alphabates +=1
    return has_alphabates        
    
my_str = "Tho455sjs"
print(count_alphabates(my_str))
    
# Q8. Ask a string from user. Print the string with first 2 letters and last 2 letters.
def print_two_letter(my_str):
    if len(my_str)<3:
        print("Not enough characters")
    else:
#    1st way (log method) using list comprehension
        
        print("First 2 letters: ",[my_str[i] for i in range(0,2)])
        print("Last 2 letters: ",[my_str[i] for i in range(len(my_str)-1,len(my_str)-3,-1)])
    
    # 2nd way by slicing shortest way   
        print("First 2 letters: ",my_str[:2])    
        print("Last 2 letters: ",my_str[-2:])
        
my_str = "it's happen only in India"    
print_two_letter(my_str)


# Q1. Write a Python program to get the 4th element from the last element of a tuple
def find_element(n):
    if len(n)>4:
        print(n[-4])
    else:
        print("Tuple size is too short")
        
my_tuple = (4,6,9,87,1,'apple')
find_element(my_tuple)
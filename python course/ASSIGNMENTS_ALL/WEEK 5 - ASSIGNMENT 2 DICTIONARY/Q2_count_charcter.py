# Q2. Create a function named countChar which will accept a string as a
# parameter. Create a dictionary having frequency of each character.

def countChar(my_str: str):
    my_dict = {}
    for i in my_str:
        if i in my_dict:
            my_dict[i] += 1
        else:
            my_dict[i] = 1
    print(my_dict)
        
my_str = "this is my str"
countChar(my_str) 
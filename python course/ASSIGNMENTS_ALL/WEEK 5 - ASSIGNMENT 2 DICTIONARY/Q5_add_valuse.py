'''Q5. Write a Python program to combine two dictionary by adding values
for common keys.
d1 = {'a': 100, 'b': 200, 'c':300}
d2 = {'a': 300, 'b': 200, 'd':400}
Sample output: {'a': 400, 'b': 400, 'd': 400, 'c': 300}'''

def sumOfValues(d1: dict, d2: dict):
    sum_dict = d1.copy()
    for key in d2:
        if key in sum_dict:
            sum_dict[key] += d2[key]
        else:
            sum_dict[key] = d2[key]
            
    return sum_dict

# OPTIMAL WAY
def combineDictionary(d1, d2):
    combined_dict = {}

    # This combines keys of both d1 and d2
    for key in d1.keys() | d2.keys():
        print(key)
        combined_dict[key] = d1.get(key, 0) + d2.get(key, 0)

    return combined_dict

d1 = {'a': 100, 'b': 200, 'c':300}
d2 = {'a': 300, 'b': 200, 'd':400}

print(combineDictionary(d1,d2))
print(sumOfValues(d1,d2))

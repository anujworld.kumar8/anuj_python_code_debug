# Q4. Write a Python function that takes two dictionaries as input, where the values are lists. 
# The function should merge the lists corresponding to the same keys in both dictionaries into a 
# single list and return a new dictionary with these merged lists

# Input dictionaries {'A': [1, 2, 3], 'B': [4, 5, 6]} 
# and {'A': [7, 8], 'B': [9, 10]} 
# Output: {'A': [1, 2, 3, 7, 8], 'B': [4, 5, 6, 9, 10]}

def merge_list_as_dict_values(dict1: dict, dict2: dict):
    new_dict = {}
    for key in dict1:
        if key in dict2:
            new_dict[key] = dict1[key] + dict2[key]
        else:
            new_dict[key] = dict1[key]
        
    print(new_dict)

dict1 = {'A': [1, 2, 3], 'B': [4, 5, 6]}
dict2 = {'A': [7, 8], 'B': [9, 10]}
merge_list_as_dict_values(dict1, dict2)
        
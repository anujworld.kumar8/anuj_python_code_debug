# Q3. Write a Python function that takes a dictionary as input where the
# values are lists of integers. The function should return a new dictionary
# where the values are lists containing only the even integers from the
# original lists.

# Input dictionary: 
# {'A': [1, 2, 3, 4], 'B': [5, 6, 7, 8])
# Output: {'A': [2, 4], 'B': [6, 8]}

def filter_even_values(my_dict: dict):
    new_dict = {}
    for x , y in  my_dict.items():
        even_number = [i for i in y if i%2==0]
        new_dict[x] = even_number
    print(new_dict)
                      
filter_even_values(my_dict= {'A': [1, 2, 3, 4], 'B': [5, 6, 7, 8]})
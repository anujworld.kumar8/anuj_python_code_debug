# Q1. Write a Python function that takes a dictionary as input where the values are lists.
# The function should return a new list containing all the elements from all the lists in the
# dictionary. It should have at least 3-4 keys and any amount of elements can be in a list.
from typing import Dict
def combine_element(my_dict: Dict):
    my_list = []
    for key , value in my_dict.items():
        # first way
        # for i in value:
            # my_list.append(i)  #
          
           # # 2nd way
        # my_list = my_list+value
        
        # #3rd way
        my_list.extend(value)
    print(my_list)    
       
student_details = {
    "Aman" : [82,96,45,67,78],
    "Ram" : [88,78,95,96,84],
    "Mohan" : [48,95,66,92,79]
}
combine_element(student_details)















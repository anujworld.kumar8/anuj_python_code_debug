'''Q1. Write a Python function that takes two numbers as input and returns the result of their division.
Handle the ZeroDivisionError exception if the second number is zero. If there is ZeroDivisionError, 
the function should return -1.'''
def division(a,b):
    try:
        div = a/b
        return div
    except ZeroDivisionError:
        print(f"Number cannot by divide by {b}")
        return -1
    
a= int(input("Enter a number: "))
b= int(input("Enter the second number: "))
print(division(a,b))
    




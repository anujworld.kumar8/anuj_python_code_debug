'''Q4. Write a function that takes a list as input and prints the element at index 5. 
Handle the IndexError exception if the list doesn't have enough elements. 
'''
def list_input(my_list: list, index:int):
    try:
        list_element = my_list[index]
        print(f"The element at index: {index} is {list_element}")
    except IndexError:
        print(f"There is no element at index: {index}")
   
my_list = [2,34,5,7,8]   
index = int(input("Enter the index value: ")) 
list_input(my_list, index)
    
    
    








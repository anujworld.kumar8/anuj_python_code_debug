'''Q2. Write a Python program that asks the user to input their age. 
Handle the ValueError exception if the user enters a non-integer value. '''

def valueErrorException():
    try:
        age = int(input("Enter the age: "))
        return age
    except ValueError:
        print("age must be a valid number")
        
age = valueErrorException()
print(age)




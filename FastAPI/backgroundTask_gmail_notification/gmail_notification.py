from fastapi import FastAPI, BackgroundTasks

app = FastAPI()

# Define a background task function
def send_email_notification(email: str, message: str):
    # Simulate sending an email (replace with real email-sending code)
    print(f"Sending email to {email}: {message}")

# Define an endpoint with BackgroundTasks
@app.post("/process-data/")
async def process_data(email: str, background_tasks: BackgroundTasks):
    # Add the background task to send an email
    background_tasks.add_task(send_email_notification, email, "Thank you for your request!")
    # Respond to the client immediately
    return {"message": "Data processing started"}

# This endpoint will respond to the client right away,
# while the email is sent in the background.